package com.example.client;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.*;
import android.widget.*;
import android.view.View.OnClickListener;

import beans.*;

import java.util.ArrayList;

public class CountryGUI extends Activity implements OnClickListener, View.OnLongClickListener {

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.post_application);

    startActivityForResult(new Intent(this, AskForWaitActivity.class), 10);

    sexArray = getResources().getStringArray(R.array.sex_array);
    (findViewById(R.id.competitionListViewButton)).setOnClickListener(this);
    (findViewById(R.id.deleteAthleteButton)).setOnClickListener(this);
    forceEdit = false;
    oldAthleteName = "";
    currentCompetition = "all competitions";
    nameTextEdit = (EditText)findViewById(R.id.nameTextEdit);
    sexSpinner = (Spinner) findViewById(R.id.sexSpinner);
    weightTextEdit = (EditText)findViewById(R.id.weightTextEdit);
    heightTextEdit = (EditText)findViewById(R.id.heightTextEdit);
    linearLayout = (LinearLayout) findViewById(R.id.linLayMain);
    selectedSports = new ArrayList<String>();
    athleteListView = new ArrayList<ClientAthlete>();
    findViewById(R.id.add_button).setOnClickListener(this);
    findViewById(R.id.postApplicationButton).setOnClickListener(this);
    findViewById(R.id.exitFromPostApplicationButton).setOnClickListener(this);
    athleteCompetitionNumber = (TextView) findViewById(R.id.athleteCompetitionNumber);
    sportSpinner = (Spinner) findViewById(R.id.competitionSpinner);

    AuthorizationData authorizationData = AuthorizationData.getInstance();
    (new ExistApplicationGetTask(authorizationData.getLogin(), authorizationData.getPassword(),
        authorizationData.getServerURL(), this)).execute();
  }

  public void onApplicationSendTask(boolean result) {
    if (result) {
      Toast.makeText(this, "application has send to server", Toast.LENGTH_SHORT).show();
    } else {
      Toast.makeText(this, "fail connection", Toast.LENGTH_SHORT).show();
    }
    finishActivity(10);
    finishActivity(11);
  }

  public boolean onCreateOptionsMenu(
      Menu
          menu) {
    menu.
        add(0,

            1, 0,
            "post application");
    return
        super.onCreateOptionsMenu(
            menu);
  }

  @Override
  public boolean onPrepareOptionsMenu(Menu menu) {
    return
        super.onPrepareOptionsMenu(
            menu
        );
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()){
      case 1:
        startActivityForResult(new Intent(this, AskForWaitActivity.class), 10);

        ArrayList<ClientCompetition> arcl = new ArrayList<ClientCompetition>();
        for (int i = 0; i < competitionNamesList.length; i++) {
          String competition = competitionNamesList[i];
          ClientCompetition clientCompetition = new ClientCompetition(competition, athleteMaxNumberList[i]);
          for (ClientAthlete clientAthlete : athleteListView) {
            if (clientAthlete.getCompetitions().contains(competition)) {
              clientCompetition.addAthlete(0, new Athlete(
                  clientAthlete.getName(), clientAthlete.getSex(),
                  clientAthlete.getWeight(), clientAthlete.getHeight(),
                  competition
              ));
            }
          }
          arcl.add(clientCompetition);
        }
        CompetitionList competitionList = new CompetitionList();
        competitionList.setCompetitionList(arcl);
        AuthorizationData data = AuthorizationData.getInstance();
        CountryApplication countryApplication = new CountryApplication(data.getLogin(), data.getPassword(), competitionList);

        (new ApplicationSendTask(countryApplication, data.getServerURL(), this)).execute();
        break;
    }
    return super.onOptionsItemSelected(item);
  }

  public void getCountryApplicationFromServer(CountryApplication result) {
    Log.d("DAN", "получили ответ от сервера. Запустился getCountryApplicationFromServer.");
    ArrayList<ClientCompetition>  compList = result.getCompetitionList().getCompetitionList();

    if (compList.size() != 0) {
      Log.d("DAN", "получили не пустую заявку.");
      athleteMaxNumberList = new int[compList.size()];
      athleteCurrentNumberList = new int[compList.size()];
      competitionNamesList = new String[compList.size()];
      sportSpinnerCompetitionsNameList = new String[compList.size() + 1];
      sportSpinnerCompetitionsNameList[0] = "all competitions";
      int i = 0;
      for (ClientCompetition competition: compList) {
        Log.d("DAN", "competition." + competition.getCompetition());
        athleteMaxNumberList[i] = competition.getMaxAthleteNumber();
        competitionNamesList[i] = competition.getCompetition();
        athleteCurrentNumberList[i] = 0;
        sportSpinnerCompetitionsNameList[i + 1] = competition.getCompetition();
        i++;

        for (Athlete athlete : competition.getAthleteCompetitionList()) {
          Log.d("DAN", "addAthlete " + athlete.getName() );
          addAthleteToAthleteListView(athlete);
        }
      }

      for (ClientAthlete athlete : athleteListView) {
        Log.d("DAN", "addRow " + athlete.getName());
        addRow(athlete.getName());
      }

      for (ClientAthlete athlete : athleteListView) {
        for(String competition : athlete.getCompetitions()) {
          for (int w = 0; w < compList.size(); w++) {
            if (competitionNamesList[w].equals(competition)) {
              if (athleteMaxNumberList[w] == athleteCurrentNumberList[w]) {
                Toast.makeText(this, "fail!incorrect country application.", Toast.LENGTH_LONG).show();
                finishActivity(10);
                this.finish();
                return;
              }
              athleteCurrentNumberList[w]++;
              w = compList.size();
            }
          }
        }
      }

      currentCompetition = "all competitions";
      sportSpinner.setAdapter(new ArrayAdapter<String>(this,
          android.R.layout.simple_spinner_item, sportSpinnerCompetitionsNameList));
      sportSpinner.setSelection(0);
      sportSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view,
                                   int selectedItem, long id) {
          forceEdit = false;
          oldAthleteName = "";
          nameTextEdit.setText("");
          sexSpinner.setSelection(0);
          weightTextEdit.setText("");
          heightTextEdit.setText("");
          selectedSports = new ArrayList<String>();

          currentCompetition = sportSpinnerCompetitionsNameList[selectedItem];
          if (selectedItem > 0 && selectedItem <= competitionNamesList.length) {
            selectedItem--;
            int athleteCurrentNumber = athleteCurrentNumberList[selectedItem];
            int athleteMaxNumber = athleteMaxNumberList[selectedItem];
            athleteCompetitionNumber.setText("осталось спортсменов: \n" + (athleteMaxNumber - athleteCurrentNumber) +
                "/" + athleteMaxNumber);
          } else {
            athleteCompetitionNumber.setText("Choose\ncomp..");
          }

          if (!currentCompetition.equals("all competitions")) {
            linearLayout.removeAllViews();
            for (ClientAthlete athlete : athleteListView) {
              if (athlete.getCompetitions().contains(currentCompetition)) {
                addRow(athlete.getName());
              }
            }
          } else {
            linearLayout.removeAllViews();
            for (ClientAthlete athlete : athleteListView) {
              addRow(athlete.getName());
            }
          }
        }

        @Override
        public void onNothingSelected(AdapterView<?> arg0) {
        }
      });


      finishActivity(10);
      Log.d("DAN", "завершсервера.");
    } else {
      Log.d("DAN", "убиваея с базы, пуста");
      try{
        finishActivity(10);
      } catch (Exception e) {
        Log.d("DAN", "поймк, то норм.");
      }
      finish();
      Log.d("DAN", "убили CountryGU, пуста");
    }
  }

  public boolean onLongClick(View v) {
    TextView tv = (TextView) v;
    String name = tv.getText() + "";
    Log.d("DAN","get athlete");
    ClientAthlete athlete = getClientAthleteFromAthleteListView(name);
    if (athlete == null) {
      return true;
    }
    Log.d("DAN","get name");
    nameTextEdit.setText(athlete.getName());
    Log.d("DAN", "get sex");
    sexSpinner.setSelection(doIndexFomSex(athlete.getSex()));
    Log.d("DAN", "get weight");
    weightTextEdit.setText(athlete.getWeight() + "");
    Log.d("DAN", "get height");
    heightTextEdit.setText(athlete.getHeight() + "");
    Log.d("DAN", "get sports " + athlete.getCompetitions().toString());
    selectedSports = athlete.getCompetitions();

    forceEdit = true;
    oldAthleteName = name;

    return true;
  }

  public void onClick(View v) {
    switch (v.getId()){
      case R.id.postApplicationButton:
        startActivityForResult(new Intent(this, AskForWaitActivity.class), 10);

        ArrayList<ClientCompetition> arcl = new ArrayList<ClientCompetition>();
        for (int i = 0; i < competitionNamesList.length; i++) {
          String competition = competitionNamesList[i];
          ClientCompetition clientCompetition = new ClientCompetition(competition, athleteMaxNumberList[i]);
          for (ClientAthlete clientAthlete : athleteListView) {
            if (clientAthlete.getCompetitions().contains(competition)) {
              clientCompetition.addAthlete(0, new Athlete(
                  clientAthlete.getName(), clientAthlete.getSex(),
                  clientAthlete.getWeight(), clientAthlete.getHeight(),
                  competition
              ));
            }
          }
          arcl.add(clientCompetition);
        }
        CompetitionList competitionList = new CompetitionList();
        competitionList.setCompetitionList(arcl);
        AuthorizationData data = AuthorizationData.getInstance();
        CountryApplication countryApplication = new CountryApplication(data.getLogin(), data.getPassword(), competitionList);

        (new ApplicationSendTask(countryApplication, data.getServerURL(), this)).execute();
        break;
      case R.id.exitFromPostApplicationButton:
        this.finish();
        break;
      case R.id.competitionListViewButton:
        Log.d("DAN","case R.id.competitionListViewButton:");

        Intent tableCountryFilterIntent = new Intent(this, TableFilter.class);
        tableCountryFilterIntent.putExtra("filterNumber", "selectSport");
        tableCountryFilterIntent.putExtra("resourceArray", competitionNamesList);

        tableCountryFilterIntent.putExtra("filterIsAlreadySelectedItems", selectedSports);

        startActivityForResult(tableCountryFilterIntent, 1);
        break;
      case R.id.add_button:
        if (!forceEdit) {
          if (getAthleteIndexFromAthleteListView(nameTextEdit.getText() + "") != -1) {
            Intent dialogActivity = new Intent(this, DialogActivity.class);
            startActivityForResult(dialogActivity, 2);
            break;
          }

          if (addAthlete()) {
            Toast.makeText(this, "Новый спортсмен добавлен", Toast.LENGTH_SHORT).show();
          }
        } else {
          if (addAthlete()) {
            Toast.makeText(this, "Информация изменена", Toast.LENGTH_SHORT).show();
            forceEdit = false;
            oldAthleteName = "";
          }
        }
        break;
      case R.id.deleteAthleteButton:
        if (forceEdit) {
          linearLayout.removeViewAt(getAthleteIndexInLinearLayout(oldAthleteName));
          removeAthleteFromAthleteListView(oldAthleteName);
          forceEdit = false; oldAthleteName = "";
          nameTextEdit.setText("");
          sexSpinner.setSelection(0);
          weightTextEdit.setText(""); heightTextEdit.setText("");
          selectedSports = new ArrayList<String>();
        } else {
          Toast.makeText(this, "удалить спортсме имени)",
              Toast.LENGTH_LONG).show();
        }
        break;
      default:
        TextView tv = (TextView) v;
        String name = tv. getText() + "";
        ClientAthlete athlete = getClientAthleteFromAthleteListView(name);
        String competitions = "";
        for (String competition : athlete.getCompetitions()) {
          competitions += competition + "\n\t";
        }

        Log.d("DAN","get athleteInformation");
        String athleteInformation = "Name: " + athlete.getName() + "\n\n" +
            "Sex: " + sexToString(athlete.getSex()) + "\n\n" +
            "Weight: " + athlete.getWeight() + "\n\n" +
            "Height: " + athlete.getHeight() + "\n\n" +
            "Competitions: " + competitions;
        Log.d("DAN","new intent");
        Intent dayActivityIntent = new Intent(this, AthleteInformationActivity.class);
        dayActivityIntent.putExtra("athleteInformation", athleteInformation);
        Log.d("DAN","startActivity");
        startActivity(dayActivityIntent);
        break;
    }
  }

  public void doFinish() {
    finishActivity(10);
    this.finish();
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (data == null) return;
    if (requestCode == 10) {
      Log.d("DAN", "перезапуск AskForWaitActivity");
      startActivityForResult(new Intent(this, AskForWaitActivity.class), 10);
    } else if (requestCode == 2) {
      if (resultCode == RESULT_OK) {
        boolean result = data.getBooleanExtra("dialogResult", false);
        if (result) {
          forceEdit = true;
          oldAthleteName = nameTextEdit.getText() + "";
          if (addAthlete()) {
            Toast.makeText(this, "Информация о спортсмене изменёна", Toast.LENGTH_SHORT).show();
            forceEdit = false;
            oldAthleteName = "";
          }
        }
      }
    } else if (requestCode == 1) {
      if (resultCode == RESULT_OK) {
        try {
          selectedSports = data.getStringArrayListExtra("resultOfChoice");
          Log.d("DAN", "selected sports: " + selectedSports.toString());

          Toast.makeText(this, selectedSports.toString(), Toast.LENGTH_LONG).show();
        } catch (Exception e) {
          Toast.makeText(this, "Неизильтрами.", Toast.LENGTH_LONG).show();
        }
      }
    }
  }

  private boolean addAthleteToAthleteListView(Athlete newAthlete) {
    for (ClientAthlete athlete : athleteListView) {
      if (athlete.getName().equals(newAthlete.getName())) {
        athlete.addCompetition(newAthlete.getCompetition());
        return true;
      }
    }
    athleteListView.add(new ClientAthlete(newAthlete));
    return false;
  }

  private void removeAthleteFromAthleteListView(String name) {
    for (ClientAthlete athlete : athleteListView) {
      if (athlete.getName().equals(name)) {
        athleteListView.remove(athlete);
        return;
      }
    }
  }

  private int getAthleteIndexFromAthleteListView(String name) {
    for (int i = 0; i < athleteListView.size(); i++) {
      if (athleteListView.get(i).getName().equals(name)) {
        return i;
      }
    }
    return -1;
  }

  private ClientAthlete getClientAthleteFromAthleteListView(String name) {
    for (ClientAthlete athlete : athleteListView) {
      if (athlete.getName().equals(name)) {
        return athlete;
      }
    }
    return null;
  }

  private int getAthleteIndexInLinearLayout(String name) {
    for ( int i = 0; i < linearLayout.getChildCount(); i++) {
      if (((TextView)linearLayout.getChildAt(i)).getText().equals(name)) {
        return i;
      }
    }
    return -1;
  }

  private void addRow(String name) {
    Log.d("DAN","in addRow");
    LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    Log.d("DAN","getting TextView");
    TextView tv = (TextView) inflater.inflate(R.layout.text_view_pattern, null);
    Log.d("DAN","getted TextView. set name");
    tv.setText(name);
    Log.d("DAN","name setted");

    tv.setOnLongClickListener(this);
    tv.setOnClickListener(this);
    tv.setGravity(Gravity.CENTER);

    if (linearLayout.getChildCount() > 0) {
      TextView tv1 = (TextView) linearLayout.getChildAt(0);
      if (tv1.getCurrentTextColor() == Color.GREEN) {
        tv.setBackgroundColor(Color.GREEN);
        tv.setTextColor(Color.BLACK);
      } else {
        tv.setBackgroundColor(Color.BLACK);
        tv.setTextColor(Color.GREEN);
      }
    }

    Log.d("DAN","linearLayout.addView(tv,index);");
    linearLayout.addView(tv,0);
    Log.d("DAN","exit addRow");
  }

  private Sex toSex(String str) {
    if (str.equals("Male") || str.equals("male") || str.equals("M") || str.equals("m")) {
      return new Sex(Sex.male);
    } else if (str.equals("Female") || str.equals("female") || str.equals("F") || str.equals("f")) {
      return new Sex(Sex.female);
    } else {
      return new Sex(Sex.undefined);
    }
  }

  private boolean addAthlete() {
    try {
      String name = nameTextEdit.getText() + "";
      Sex sex = toSex(sexArray[sexSpinner.getSelectedItemPosition()]);
      int weight = Integer.parseInt((weightTextEdit.getText() + ""));
      int height = Integer.parseInt((heightTextEdit.getText() + ""));
      if (weight < 20 || weight > 200) {
        Toast.makeText(this, "Вес введены некорректно. 20..200", Toast.LENGTH_SHORT).show();
        return false;
      } else if (height < 100 || height > 250){
        Toast.makeText(this, "Вес или рост введены некорректно. 100..250", Toast.LENGTH_SHORT).show();
        return false;
      }

      if(forceEdit){
        Log.d("DAN", "addInLinearLayout(name)); " +  oldAthleteName);
        linearLayout.removeViewAt(getAthleteIndexInLinearLayout(oldAthleteName));
        Log.d("DAN", "addView(oldAthleteName);" + oldAthleteName);
        removeAthleteFromAthleteListView(oldAthleteName);
        forceEdit = false;
      }
      athleteListView.add(new ClientAthlete(name, sex, weight, height, selectedSports));
      // меняем число человек в заявке
      int[] newAthleteCurrentNumberList = athleteCurrentNumberList;
      for (String comp : selectedSports) {
        for (int i = 0; i < competitionNamesList.length; i++) {
          if (competitionNamesList[i].equals(comp)) {
            if (athleteMaxNumberList[i] == newAthleteCurrentNumberList[i]) {
              Toast.makeText(this, "вы исчерревнованию " + comp, Toast.LENGTH_SHORT).show();
              return false;
            }
            newAthleteCurrentNumberList[i]++;
            i = competitionNamesList.length;
          }
        }
      }
      athleteCurrentNumberList = newAthleteCurrentNumberList;

      if (selectedSports.contains(currentCompetition) || currentCompetition.equals("all competitions")) {
        addRow(name);
      }

      int selectedItem = sportSpinner.getSelectedItemPosition();
      currentCompetition = sportSpinnerCompetitionsNameList[selectedItem];
      if (selectedItem > 0) {
        selectedItem--;
        int athleteCurrentNumber = athleteCurrentNumberList[selectedItem];
        int athleteMaxNumber = athleteMaxNumberList[selectedItem];
        athleteCompetitionNumber.setText("Осталось: " + (athleteMaxNumber - athleteCurrentNumber) +
            "/" + athleteMaxNumber);
      } else {
        athleteCompetitionNumber.setText("Choose\ncomp..");
      }
    } catch (NumberFormatException e) {
      Toast.makeText(this, "Вес или рост введены некорректно.", Toast.LENGTH_SHORT).show();
      return false;
    }

    nameTextEdit.setText("");
    sexSpinner.setSelection(0);
    weightTextEdit.setText(""); heightTextEdit.setText("");
    selectedSports = new ArrayList<String>();
    forceEdit = false; oldAthleteName = "";
    return true;
  }

  private String sexToString(Sex sex) {
    switch (sex.getSex()) {
      case Sex.undefined:
        return "undefined";
      case Sex.male:
        return "male";
      case Sex.female:
        return "female";
      default:
        return "undefined";
    }
  }

  private int doIndexFomSex(Sex sex) {
    switch (sex.getSex()) {
      case Sex.male:
        return 1;
      case Sex.female:
        return 2;
      default:
        return 0;
    }
  }

  private boolean forceEdit;

  private String oldAthleteName;
  private EditText nameTextEdit;
  private Spinner sexSpinner;
  private String[] sexArray;
  private EditText weightTextEdit;
  private EditText heightTextEdit;
  private ArrayList<String> selectedSports;
  private ArrayList<ClientAthlete> athleteListView;
  private Spinner sportSpinner;
  private String[] sportSpinnerCompetitionsNameList;
  private String currentCompetition;
  private TextView athleteCompetitionNumber;
  private LinearLayout linearLayout;
  private int[] athleteCurrentNumberList;
  private int[] athleteMaxNumberList;
  private String[] competitionNamesList;
}