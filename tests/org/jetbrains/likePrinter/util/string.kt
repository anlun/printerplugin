package org.jetbrains.likePrinter.util.string

import org.junit.Test
import kotlin.test.assertEquals
import org.jetbrains.likePrinter.util.string.*
import kotlin.test.assertFalse
import kotlin.test.assertTrue
import com.intellij.openapi.util.TextRange
import org.jetbrains.likePrinter.util.base.InsertPlace
import org.jetbrains.format.util.toLines
import org.jetbrains.format.util.getIndentation
import org.jetbrains.format.util.startWhitespaceLength

/**
 * User: anlun
 */

public class StringTest {
    Test fun maxDropSpaceNumber_Test() {
        val str = "a\n  b\n   c\n d"
        val startLineOffset = 3
        val expected = 1
        assertEquals(expected, str.maxDropSpaceNumber(startLineOffset), "Incorrect drop length for non-empty string.")
    }

    Test fun maxDropSpaceNumber_Empty() {
        val str = ""
        val startLineOffset = 3
        val expected = 0
        assertEquals(expected, str.maxDropSpaceNumber(startLineOffset), "Incorrect drop length for empty string.")
    }

    Test fun deleteSpaces_Empty() {
        val str = ""
        val expected = str
        val startLineOffset = 0
        val result = str.deleteSpaces(startLineOffset)
        assertEquals(expected, result, "Incorrect delete spaces for empty string.")
    }

    Test fun deleteSpaces_Test() {
        val str = "a\n  b\n   c\n    d"
        val expected = "a\nb\n c\n  d"
        val startLineOffset = 3
        val result = str.deleteSpaces(startLineOffset)
        assertEquals(expected, result, "Incorrect delete spaces for non-empty string.")
    }

    Test fun isLineStart_Empty() {
        val str = ""
        val offset = 10
        assertFalse(str.isLineStart(offset))
    }

    Test fun isLineStart_True() {
        val str = "a\n  bc\n d"
        val offset = str.indexOf('b')
        assertTrue(str.isLineStart(offset))
    }

    Test fun isLineStart_False() {
        val str = "a\n  bc\n d"
        val offset = str.indexOf('c')
        assertFalse(str.isLineStart(offset))
    }

    private fun getFillConstant_Test(str: String): Int {
        val aPos  = str.indexOf('a')
        val bPos  = str.indexOf('B')
        val range = TextRange(aPos, bPos)

        return str.getFillConstant(range)
    }

    Test fun getFillConstant_PositiveFill() {
        val str =
                "___a\n" +
                "  aa\n" +
                " aB"
        val expected = 1
        val result = getFillConstant_Test(str)

        assertEquals(expected, result)
    }

    Test fun getFillConstant_0Fill() {
        val str =
                "___a\n" +
                "aa\n" +
                "aB"
        val expected = InsertPlace.DOESNT_START_WITH_NEW_LINE
        val result = getFillConstant_Test(str)

        assertEquals(expected, result)
    }

    Test fun getFillConstant_NewLine() {
        val str =
                "____\n"  +
                "  a\n"   +
                "   aa\n" +
                "   aB"
        val expected = InsertPlace.STARTS_WITH_NEW_LINE
        val result = getFillConstant_Test(str)

        assertEquals(expected, result)
    }

    Test fun getOffsetInLine_Test() {
        val str =
                "____\n"  +
                "  a\n"   +
                "   aa\n" +
                "   aB"
        val aPos = str.indexOf('a')
        val expected = 2

        val result = str.getOffsetInLine(aPos)
        assertEquals(expected, result)
    }

    Test fun offsetInLine_Empty() {
        val str      = ""
        val offset   = 10
        val expected =  0

        assertEquals(expected, str.getOffsetInLine(offset))
    }

    Test fun offsetInLine_FirstLine() {
        val str =
                "a \n" +
                " b\n" +
                "c\n"
        val offset   = str.indexOf('a')
        val expected = 0

        assertEquals(expected, str.getOffsetInLine(offset))
    }

    Test fun offsetInLine_SecondLine() {
        val str =
                "a \n" +
                " b\n" +
                "c\n"
        val offset   = str.indexOf('b')
        val expected = 1

        assertEquals(expected, str.getOffsetInLine(offset))
    }

    Test fun offsetInLine_ThirdLine() {
        val str =
                "a \n" +
                " b\n" +
                "c\n"
        val offset   = str.indexOf('c')
        val expected = 0

        assertEquals(expected, str.getOffsetInLine(offset))
    }

    Test fun toRanges_Empty() {
        val str = ""
        val lines = str.toLines()
        assertEquals(0, lines.toRanges().size())
    }

    private fun toRanges_test(str: String, expected: List<Pair<Int, Int>>) {
        val lines = str.toLines()
        val ranges = lines.toRanges().map { tr -> Pair(tr.getStartOffset(), tr.getEndOffset()) }
        assertEquals(expected, ranges)
    }

    Test fun toRanges_2Lines() {
        val str = "a\nb"
        val expected = listOf(Pair(0, 1), Pair(2, 3))
        toRanges_test(str, expected)
    }

    Test fun toRanges_3Lines() {
        val str = "a\nb\nc"
        val expected = listOf(Pair(0, 1), Pair(2, 3), Pair(4, 5))
        toRanges_test(str, expected)
    }

    Test fun toRanges_4Lines() {
        val str = "a\nb\nc\n"
        val expected = listOf(Pair(0, 1), Pair(2, 3), Pair(4, 5), Pair(6, 6))
        toRanges_test(str, expected)
    }

    Test fun getTagPlaceToLineNumberMap() {
        val text =
            "a\n"   +
            "  b\n" +
            "  c\n" +
            "d\n"
        val indexOfA = text.indexOf("a")
        val indexOfB = text.indexOf("b")
        val indexOfC = text.indexOf("c")
        val indexOfD = text.indexOf("d")

        val rangeA  = TextRange(indexOfA, indexOfA + 1)
        val rangeBC = TextRange(indexOfB, indexOfC + 1)
        val rangeD  = TextRange(indexOfD, indexOfD + 1)

        val lines = text.toLines()
        val expected = mapOf(
                  Pair(TagPlaceLine("A" , LinePosition.FIRST), 0)
                , Pair(TagPlaceLine("BC", LinePosition.FIRST), 1)
                , Pair(TagPlaceLine("BC", LinePosition. LAST), 2)
                , Pair(TagPlaceLine("D" , LinePosition.FIRST), 3)
        )
        val result = lines.getTagPlaceToLineNumberMap(
                mapOf(
                          Pair("A" , rangeA )
                        , Pair("BC", rangeBC)
                        , Pair("D" , rangeD )
                )
        )

        assertEquals(expected, result)
    }

    Test fun getLineEquations_1() {
        val text =
                "a\n"   +
                "  b\n" +
                "  c\n" +
                "d\n"
        val indexOfA = text.indexOf("a")
        val indexOfB = text.indexOf("b")
        val indexOfC = text.indexOf("c")
        val indexOfD = text.indexOf("d")

        val rangeA  = TextRange(indexOfA, indexOfA + 1)
        val rangeBC = TextRange(indexOfB, indexOfC + 1)
        val rangeD  = TextRange(indexOfD, indexOfD + 1)

        val lines = text.toLines()
        val tagToRangeMap = mapOf(
                  Pair("A", rangeA)
                , Pair("BC", rangeBC)
                , Pair("D", rangeD)
        )
        val tagPlaceToLineNumberMap = lines.getTagPlaceToLineNumberMap(tagToRangeMap)

        val expected = mapOf(
                  Pair(0
                    , LineEquation(0, setOf(TagPlaceLine("A", LinePosition.FIRST)))
                  )
                , Pair(1
                    , LineEquation(2, setOf(TagPlaceLine("BC", LinePosition.FIRST)))
                  )
                , Pair(2
                    , LineEquation(0, setOf(TagPlaceLine("BC", LinePosition.LAST)))
                  )
                , Pair(3
                    , LineEquation(0, setOf(TagPlaceLine("D", LinePosition.FIRST)))
                  )
        )

        val result = lines.getLineEquations(tagToRangeMap, tagPlaceToLineNumberMap)
        assertEquals(expected, result)
    }

    Test fun getLineEquations_2() {
        val text =
                "a\n"   +
                "  b c\n" +
                "  d turw\n" +
                "__f\n"
        val indexOfA = text.indexOf("a")
        val indexOfB = text.indexOf("b")
        val indexOfC = text.indexOf("c")
        val indexOfD = text.indexOf("d")
        val indexOfF = text.indexOf("f")

        val rangeA  = TextRange(indexOfA, indexOfA + 1)
        val rangeB  = TextRange(indexOfB, indexOfB + 1)
        val rangeCD = TextRange(indexOfC, indexOfD + 1)
        val rangeF  = TextRange(indexOfF, indexOfF + 1)

        val lines = text.toLines()
        val tagToRangeMap = mapOf(
                  Pair("A" , rangeA )
                , Pair("B" , rangeB )
                , Pair("CD", rangeCD)
                , Pair("F" , rangeF )
        )
        val tagPlaceToLineNumberMap = lines.getTagPlaceToLineNumberMap(tagToRangeMap)

        val expected = mapOf(
                Pair(0
                        , LineEquation(0, setOf(TagPlaceLine("A", LinePosition.FIRST)))
                )
                , Pair(1
                        , LineEquation(3, setOf(TagPlaceLine("B", LinePosition.FIRST), TagPlaceLine("CD", LinePosition.FIRST)))
                )
                , Pair(2
                        , LineEquation(" turw".length(), setOf(TagPlaceLine("CD", LinePosition.LAST)))
                )
                , Pair(3
                        , LineEquation("__".length(), setOf(TagPlaceLine("F", LinePosition.FIRST)))
                )
        )

        val result = lines.getLineEquations(tagToRangeMap, tagPlaceToLineNumberMap)
        assertEquals(expected, result)
    }

    Test fun lineEndTrim_Empty() {
        val text = ""
        assertEquals(text, text.lineEndTrim())
    }

    Test fun lineEndTrim_OneLine() {
        val text = " a "
        val expected = " a"
        assertEquals(expected, text.lineEndTrim())
    }

    Test fun lineEndTrim_TwoLine() {
        val text = " a \n b "
        val expected = " a\n b"
        assertEquals(expected, text.lineEndTrim())
    }

    Test fun lineEndTrim_TwoLineEmpty() {
        val text = " a \n b \n"
        val expected = " a\n b\n"
        assertEquals(expected, text.lineEndTrim())
    }

    Test fun replace_Single() {
        val text = "a"
        val result = text.replaceMultiple(listOf(Pair(TextRange(0, 1), "replacement")))
        val expected = "replacement"
        assertEquals(expected, result)

    }

    Test fun replace_Multiple() {
        val text = "a b c d"
        val aIndexOf = text.indexOf("a")
        val aRange = TextRange(aIndexOf, aIndexOf + 1)
        val bIndexOf = text.indexOf("b")
        val bRange = TextRange(bIndexOf, bIndexOf + 1)
        val cIndexOf = text.indexOf("c")
        val cRange = TextRange(cIndexOf, cIndexOf + 1)

        val result = text.replaceMultiple(listOf(
                  Pair(aRange, "11")
                , Pair(bRange, "22")
                , Pair(cRange, "33")
        ))

        val expected = "11 22 33 d"
        assertEquals(expected, result)
    }

    Test fun replace_Multiple_2() {
        val text = "tsdf a b c d"
        val aIndexOf = text.indexOf("a")
        val aRange = TextRange(aIndexOf, aIndexOf + 1)
        val bIndexOf = text.indexOf("b")
        val bRange = TextRange(bIndexOf, bIndexOf + 1)
        val cIndexOf = text.indexOf("c")
        val cRange = TextRange(cIndexOf, cIndexOf + 1)

        val result = text.replaceMultiple(listOf(
                  Pair(aRange, "11")
                , Pair(bRange, "22")
                , Pair(cRange, "33")
        ))

        val expected = "tsdf 11 22 33 d"
        assertEquals(expected, result)
    }

    Test fun replaceIndentTabs() {
        val text     = "\t\ta\n\tb"
        val expected = "    a\n  b"
        assertEquals(expected, text.replaceIndentTabs(2))
    }

}