package org.jetbrains.likePrinter.components

import org.jetbrains.likePrinter.readFile
import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.util.string.lineEndTrim
import org.junit.Test
import kotlin.test.assertEquals
import com.intellij.psi.PsiJavaFile
import com.intellij.testFramework.fixtures.LightCodeInsightFixtureTestCase

/**
 * User: anlun
 */
public class JavaCodeReferenceTest: ComponentTest() {
    final val OUR_PATH_TO_COMPONENTS: String
        get() = PATH_TO_COMPONENTS + "references/"

    final val OUR_MAX_WIDTH: Int
        get() = 85

    override fun getOurPathToComponents(): String = OUR_PATH_TO_COMPONENTS
    override fun getOurMaxWidth(): Int = OUR_MAX_WIDTH;

    Test fun test_1() {
        test("CodeReferenceT.java", "CodeReferenceEx.java", "CodeReferenceExpected.java")
    }

    Test fun test_11() {
        test("CodeReferenceT_Brackets.java", "CodeReferenceEx.java", "CodeReferenceExpected.java")
    }

    Test fun test_2() {
        test("CodeReferenceT.java", "CodeReferenceEx_2.java", "CodeReferenceExpected_2.java")
    }

    Test fun test_22() {
        test("CodeReferenceT.java", "CodeReferenceEx_2.java", "CodeReferenceExpected_2.java")
    }
}