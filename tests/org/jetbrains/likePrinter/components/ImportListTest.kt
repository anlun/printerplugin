package org.jetbrains.likePrinter.components

import org.jetbrains.likePrinter.readFile
import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.util.string.lineEndTrim
import com.intellij.psi.PsiFileFactory
import com.intellij.psi.PsiJavaFile
import org.junit.Test
import kotlin.test.assertEquals

/**
 * User: anlun
 */
public class ImportListTest: ComponentTest() {
    final val OUR_PATH_TO_COMPONENTS: String
        get() = PATH_TO_COMPONENTS + "import/"

    override fun getOurPathToComponents(): String = OUR_PATH_TO_COMPONENTS;

    Test fun test_simple() {
        test("ImportT.java", "ImportEx.java", "ImportExpected.java")
    }
    Test fun test_static() {
        test("ImportT_static.java", "ImportEx_static.java", "ImportExpected_static.java")
    }
    Test fun test_multi() {
        test("ImportT_multi.java", "ImportEx_multi.java", "ImportExpected_multi.java")
    }
}