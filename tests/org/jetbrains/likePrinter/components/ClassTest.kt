package org.jetbrains.likePrinter.components

import org.jetbrains.likePrinter.readFile
import org.jetbrains.likePrinter.printer.Printer
import com.intellij.psi.PsiJavaFile
import org.jetbrains.likePrinter.util.string.lineEndTrim
import org.jetbrains.likePrinter.printer.createPrinterByFile
import com.intellij.openapi.vfs.VirtualFile
import com.intellij.openapi.vfs.VirtualFileManager
import kotlin.test.assertEquals
import com.intellij.openapi.vfs.LocalFileSystem
import org.jetbrains.likePrinter.printer.PrinterSettings
import org.jetbrains.format.FormatSet.FormatSetType
import org.jetbrains.format.FormatSet
import com.intellij.openapi.project.Project
import org.junit.Before
import org.junit.Test
import java.lang.management.ManagementFactory
import com.intellij.psi.PsiClass

/**
 * User: anlun
 */
public class ClassTest: ComponentTest() {
    final val OUR_MAX_WIDTH: Int
        get() = 79

    final val OUR_LITTLE_WIDTH: Int
        get() = 150

    final val OUR_BIG_WIDTH: Int
        get() = 250


    override fun getOurPathToComponents(): String = PATH_TO_COMPONENTS + "class/"

    //final val tmpltFilePath: String = "testData/templateRepo"
    final val tmpltFilePath: String = "/home/user/gitRepos/intellij-community"

    var myPrinter: Printer? = null
    var myProject: Project? = null

    Test override public fun setUp() {
        super<ComponentTest>.setUp()
        FormatSet.setDefaultSettings()
        if (myPrinter == null) {
            myProject = getProject()
            val file = LocalFileSystem.getInstance()?.findFileByPath(tmpltFilePath)!!
            myPrinter = createPrinterByFile(file, PrinterSettings.createProjectSettings(239, myProject!!), 4)
        } else {
            myPrinter?.clearCache()
        }
    }

    // For performance test
    private fun getVariants(pr: Printer, classToReprint: PsiClass) = pr.getVariants(classToReprint)

    private fun test1(
            classToReprintFileName: String, settings: PrinterSettings, resultWidth: Int = settings.width
    ): Pair<Double, String> {
        val toReprintFileContent = readFile(getOurPathToComponents() + classToReprintFileName).trim()
        val factory = getElementFactory()
        val classToReprint = factory!!.createClassFromText(toReprintFileContent, null)
                                .getAllInnerClasses()[0]

        myPrinter?.setMaxWidth                    (settings.width)
        myPrinter?.setFormatSetType               (settings.formatSetType)
        myPrinter?.setMultipleListElemVariantNeeds(settings.multipleListElemVariants)
        myPrinter?.setMultipleExprStmtVariantNeeds(settings.multipleExprStmtVariants)

        val threadMXBean = ManagementFactory.getThreadMXBean()!!
        val startTime = threadMXBean.getCurrentThreadCpuTime()
        val variants  = getVariants(myPrinter!!, classToReprint)
        val endTime   = threadMXBean.getCurrentThreadCpuTime()

        val format = variants.head(resultWidth)
        val text   = if (format != null) format.toText(0, "") else ""

        val duration = (endTime - startTime) / Math.pow(10.0, 9.0)
        if (text != "") {
            println(duration)
        } else {
            println("fail $duration")
        }

        return Pair(duration, text.lineEndTrim())
    }

    Test public fun testAA() { megaTest() }

    private fun typeToStr(fmtType: FormatSetType): String =
        when (fmtType) {
            FormatSetType.D1          -> "D1  "
            FormatSetType.D2          -> "D2  "
            FormatSetType.D3          -> "D3  "
            FormatSetType.D3AF        -> "D3AF"
            FormatSetType.List        -> "List"
            FormatSetType.SteppedD3AF -> "D3AF"
            else                      -> "unkn"
        }

    private fun boolToStr(b: Boolean): String = if (b) "true " else "false"

    private fun megaTest() {
        val resultWidths = listOf(200, 250)
        val classToReprintFileNames: List<String> = listOf("Messages.java")
//        val classToReprintFileNames: List<String> = listOf("Messages.java", "EditorImpl.java")
//        val classToReprintFileNames: List<String> = listOf("Messages.java", "UIUtil.java", "AbstractTreeUi.java", "EditorImpl.java", "ConcurrentHashMap.java")
//        val classToReprintFileNames: List<String> = listOf(
//                "Messages.java", "EditorImpl.java"
//              , "SearchRequestCollector.java"
//              , "XDebugProcess.java"
//              , "InitialConfigurationDialog.java"
//              , "QuickEditHandler.java"
//              , "PsiDirectoryImpl.java"
//              , "Messages.java", "UIUtil.java", "AbstractTreeUi.java", "EditorImpl.java", "ConcurrentHashMap.java"
//        )
        val ftList = listOf(FormatSetType.D3AF/*, FormatSetType.SteppedD3AF*/)
        val mmList = listOf(Pair(true, true))

        for (fileName in classToReprintFileNames) {
            println("$fileName\n-----")
            for (width in resultWidths) {
                println("w: $width")
                for (multiSettings in mmList)
                for (insertType    in listOf(true/*, false*/)) {
                    println("\tit: $insertType")
                    for (formatType in ftList) {
                        FormatSet.usingNewInsertToText = insertType
                        FormatSet.defaultFormatSetType = formatType
                        val steps = if (formatType == FormatSetType.SteppedD3AF) { listOf(3, 5, 7) } else { listOf(1) }

                        for (step in steps) {
                            FormatSet.stepInMap = step
                            print("\t\tst: $step   t: ")
                            test1(
                                  fileName
                                , PrinterSettings(width, myProject!!, formatType, multiSettings.first, multiSettings.second)
                                , width
                            )
                        }
                    }
                }
            }
        }
    }

    /*
    Test fun testUnFinal() {
        val project = getProject()!!
        val settings = PrinterSettings(OUR_MAX_WIDTH, project)
        val res = test1("NonFinalClassEx.java", settings);
        assert(readFile(getOurPathToComponents() + "NonFinalClassExpected.java") == res.second)
    }

    Test fun testA_1() {
        val project = getProject()!!
        val settings = PrinterSettings(OUR_MAX_WIDTH, project)
        val res = test1("ClassEx.java", settings)
        assert("" != res.second)
    }

    Test fun testA_2() {
        val project = getProject()!!
        val settings = PrinterSettings(60, project)
        val res = test1("ClassPluginEx.java", settings)
        assert("" != res.second)
    }
    */
}