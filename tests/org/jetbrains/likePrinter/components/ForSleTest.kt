package org.jetbrains.likePrinter.components

import com.intellij.psi.PsiJavaFile
import com.intellij.testFramework.fixtures.LightCodeInsightFixtureTestCase
import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.readFile
import org.junit.Test
import kotlin.test.assertEquals

/**
 * User: anlun
 */
public class ForSleTest: LightCodeInsightFixtureTestCase() {
    fun test(templateFileName: String, toReprintFileName: String, expectedFileName: String) {
        val methodTemplateFileName = "forSLE/" + templateFileName
        val templateFileContent = readFile(methodTemplateFileName)

        val factory = getElementFactory()
        val tmpltPsiClass = factory!!.createClassFromText(templateFileContent, null)

        val project = getProject() ?: throw NullPointerException()

        val methodToReprintFileName = "forSLE/" + toReprintFileName
        val toReprintFileContent = readFile(methodToReprintFileName).trim()
        val methodToReprint = factory.createMethodFromText(toReprintFileContent, null)

        val printer = Printer.create(tmpltPsiClass.getContainingFile() as PsiJavaFile, project, 150)
        val variants = printer.getVariants(methodToReprint)

//        assertEquals(1, variants.size(), "More then one variant!")
        val format = variants.head()
        val text = if (format != null) format.toText(0, "") else ""

        val expectedResultFileName = "forSLE/" + expectedFileName
        val expectedResult = readFile(expectedResultFileName)
        assertEquals(expectedResult.trim(), text.trim(), "Incorrect result!")
    }

    Test fun test1() { test("MethodT.java", "MethodEx.java", "MethodExpected.java") }
    Test fun testw() { test("MethodT2.java", "MethodEx.java", "MethodExpected2.java") }
}