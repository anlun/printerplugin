package org.jetbrains.likePrinter.components

import org.jetbrains.likePrinter.readFile
import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.util.string.lineEndTrim
import com.intellij.psi.PsiJavaFile
import org.junit.Test
import kotlin.test.assertEquals

/**
 * User: anlun
 */
public class StmtTest : ComponentTest() {
    override fun getOurPathToComponents(): String = PATH_TO_COMPONENTS + "statements/"

    fun getTestStatement(
               templateFileName: String
            , toReprintFileName: String
    ): String {
        val templateFileName = getOurPathToComponents() + templateFileName
        val templateFileContent = readFile(templateFileName)

        val factory = getElementFactory()
        val tmpltPsiClass = factory!!.createClassFromText(templateFileContent, null)

        val project = getProject()
        if (project == null) { throw NullPointerException() }

        val toReprintFileName = getOurPathToComponents() + toReprintFileName
        val toReprintFileContent = readFile(toReprintFileName).trim()
        val stmtToReprint =
                factory.createStatementFromText(toReprintFileContent, null)

        val printer = Printer.create(tmpltPsiClass.getContainingFile() as PsiJavaFile, project, MAX_WIDTH)
        val variants = printer.getVariants(stmtToReprint)

        //        assertEquals(1, variants.size(), "More then one variant!")
        val format = variants.head()
        val text = if (format != null) format.toText(0, "") else ""

        return text.lineEndTrim()
    }

    private fun testBody(templateFileName: String, toReprintFileName: String, expectedFileName: String) {
        val text = getTestStatement(templateFileName, toReprintFileName)
        val expectedResultFileName = getOurPathToComponents() + expectedFileName
        val expectedResult = readFile(expectedResultFileName)
        assertEquals(expectedResult.trim(), text, "Incorrect result!")
    }

    Test fun testIf_1() { testBody("IfT.java", "IfEx_1.java", "IfExpected_1.java") }
    Test fun testIf_2() { testBody("IfT.java", "IfEx_2.java", "IfExpected_2.java") }
    Test fun testIf_3() { testBody("IfT.java", "IfEx_3.java", "IfExpected_3.java") }
    Test fun testIf_4() { testBody("IfT.java", "IfEx_4.java", "IfExpected_4.java") }
    Test fun testIf_5() { testBody("IfT.java", "IfEx_5.java", "IfExpected_5.java") }

    Test fun testForeach_1() { testBody("IfT.java", "ForeachEx_1.java", "ForeachExpected_1.java") }
}