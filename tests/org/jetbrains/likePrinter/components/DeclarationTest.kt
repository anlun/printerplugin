package org.jetbrains.likePrinter.components

import org.junit.Test
import com.intellij.testFramework.fixtures.LightCodeInsightFixtureTestCase
import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.readFile
import kotlin.test.assertEquals
import org.jetbrains.likePrinter.util.string.lineEndTrim
import com.intellij.psi.PsiDeclarationStatement
import com.intellij.psi.PsiVariable
import com.intellij.openapi.project.Project
import com.intellij.psi.PsiClass
import com.intellij.psi.PsiJavaFile

/**
 * User: anlun
 */
public class DeclarationTest: ComponentTest() {
    final val OUR_MAX_WIDTH: Int
        get() = 60

    override fun getOurPathToComponents(): String = PATH_TO_COMPONENTS

    fun getTestDeclaration(
               declarationTemplateFileName: String
            , declarationToReprintFileName: String
    ): String {
        val declarationTemplateFileName = PATH_TO_COMPONENTS + declarationTemplateFileName
        val templateFileContent = readFile(declarationTemplateFileName)

        val factory = getElementFactory()
        val tmpltPsiClass = factory!!.createClassFromText(templateFileContent, null)

        val project = getProject()
        if (project == null) { throw NullPointerException() }

        val declarationToReprintFileName = PATH_TO_COMPONENTS + declarationToReprintFileName
        val toReprintFileContent = readFile(declarationToReprintFileName).trim()
        val classToReprint =
                factory.createClassFromText(toReprintFileContent, null)
                        .getAllInnerClasses()[0]

        val printer = Printer.create(tmpltPsiClass.getContainingFile() as PsiJavaFile, project, OUR_MAX_WIDTH)
        val variants = printer.getVariants(classToReprint)

        //        assertEquals(1, variants.size(), "More then one variant!")
        val format = variants.head()
        val text = if (format != null) format.toText(0, "") else ""

        return text.lineEndTrim()
    }

    Test fun testDeclaration_1() {
        val text = getTestDeclaration("DeclarationT.java", "DeclarationEx.java")
        val expectedResultFileName = PATH_TO_COMPONENTS + "DeclarationExpected.java"
        val expectedResult = readFile(expectedResultFileName)
        assertEquals(expectedResult.lineEndTrim(), text, "Incorrect result!")
    }
    /*
    Test fun testDeclaration_2() {
        val text = getTestDeclaration("DeclarationT_1.java", "DeclarationEx_1.java")
        val expectedResultFileName = PATH_TO_COMPONENTS + "DeclarationExpected_1.java"
        val expectedResult = readFile(expectedResultFileName)
        assertEquals(expectedResult.trim(), text, "Incorrect result!")
    }
    */

    fun getTestDeclaration_Reprint(
             declarationTemplateFileName: String
          , declarationToReprintFileName: String
    ): String {
        val declarationTemplateFileName = PATH_TO_COMPONENTS + declarationTemplateFileName
        val templateFileContent = readFile(declarationTemplateFileName)

        val factory = getElementFactory()
        val tmpltPsiClass = factory!!.createClassFromText(templateFileContent, null)

        val project = getProject()
        if (project == null) { throw NullPointerException() }

        val declarationToReprintFileName = PATH_TO_COMPONENTS + declarationToReprintFileName
        val toReprintFileContent = readFile(declarationToReprintFileName).trim()
        val classToReprint =
                factory.createClassFromText(toReprintFileContent, null)
                        .getAllInnerClasses()[0]

        val printer = Printer.create(tmpltPsiClass.getContainingFile() as PsiJavaFile, project, OUR_MAX_WIDTH)
        val variants = printer.getVariants(classToReprint)

        val head = variants.head()
        val text = if (head != null) head.toText(0, "") else ""
        return text.lineEndTrim()
    }

    Test fun testDeclaration_Reprint_1() {
        val text = getTestDeclaration_Reprint("DeclarationT_1.java", "DeclarationEx_1.java")
        val expectedResultFileName = PATH_TO_COMPONENTS + "DeclarationExpected_1.java"
        val expectedResult = readFile(expectedResultFileName).lineEndTrim()
        assertEquals(expectedResult, text, "Incorrect result!")
    }

    Test fun testDeclaration_Reprint_2() {
        val text = getTestDeclaration_Reprint("DeclarationT_1.java", "DeclarationEx_2.java")
        val expectedResultFileName = PATH_TO_COMPONENTS + "DeclarationExpected_2.java"
        val expectedResult = readFile(expectedResultFileName)
        assertEquals(expectedResult.trim(), text, "Incorrect result!")
    }

    Test fun testDeclaration_Reprint_3() {
        val text = getTestDeclaration_Reprint("DeclarationT_1.java", "DeclarationEx_3.java")
        val expectedResultFileName = PATH_TO_COMPONENTS + "DeclarationExpected_3.java"
        val expectedResult = readFile(expectedResultFileName)
        assertEquals(expectedResult.trim(), text, "Incorrect result!")
    }
}