package org.jetbrains.likePrinter.printer

import com.intellij.psi.PsiClass
import com.intellij.openapi.project.Project

import com.intellij.psi.PsiElement
import com.intellij.psi.PsiMethod
import com.intellij.psi.PsiSynchronizedStatement
import org.jetbrains.likePrinter.util.base.*
import java.util.ArrayList
import com.intellij.psi.PsiIfStatement
import com.intellij.psi.PsiWhileStatement
import com.intellij.psi.PsiDoWhileStatement
import com.intellij.psi.PsiForStatement
import com.intellij.psi.PsiForeachStatement
import com.intellij.psi.PsiSwitchStatement
import com.intellij.psi.PsiCodeBlock
import com.intellij.psi.PsiTryStatement
import com.intellij.psi.PsiReferenceList
import com.intellij.psi.PsiParameterList
import com.intellij.psi.PsiBlockStatement
import com.intellij.psi.JavaPsiFacade
import com.intellij.openapi.application.ApplicationManager
import com.intellij.psi.PsiLambdaExpression
import com.intellij.psi.PsiExpression
import com.intellij.psi.PsiJavaFile
import com.intellij.psi.PsiClassInitializer
import com.intellij.psi.PsiParameter
import com.intellij.psi.PsiAnonymousClass
import com.intellij.psi.PsiNewExpression
import com.intellij.psi.PsiEnumConstantInitializer
import com.intellij.psi.PsiEnumConstant
import com.intellij.psi.PsiResourceList
import com.intellij.psi.PsiTypeParameter
import com.intellij.psi.PsiTypeParameterList
import com.intellij.psi.PsiVariable
import com.intellij.psi.PsiResourceVariable
import org.jetbrains.likePrinter.util.psiElement.*
import com.intellij.openapi.command.impl.DummyProject
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import org.jetbrains.likePrinter.components.statements.DoWhileComponent
import org.jetbrains.likePrinter.components.statements.WhileComponent
import org.jetbrains.likePrinter.components.statements.ForComponent
import org.jetbrains.likePrinter.components.statements.ForeachComponent
import org.jetbrains.likePrinter.components.statements.IfComponent
import org.jetbrains.likePrinter.components.statements.SwitchComponent
import org.jetbrains.likePrinter.components.statements.SynchronizedComponent
import org.jetbrains.likePrinter.components.MethodComponent
import org.jetbrains.likePrinter.components.CodeBlockComponent
import org.jetbrains.likePrinter.components.statements.TryComponent
import org.jetbrains.likePrinter.templateBase.template.Template
import org.jetbrains.likePrinter.components.expressions.LambdaComponent
import org.jetbrains.likePrinter.components.classes.ClassInitializerComponent
import org.jetbrains.likePrinter.templateBase.template.PsiTemplateGen
import org.jetbrains.likePrinter.components.classes.ClassComponent
import org.jetbrains.likePrinter.components.classes.AnonymousClassComponent
import org.jetbrains.likePrinter.components.classes.EnumConstantInitializerComponent
import org.jetbrains.likePrinter.components.classes.EnumConstantComponent
import org.jetbrains.likePrinter.components.lists.ReferenceListComponent
import org.jetbrains.likePrinter.components.lists.ListTemplate
import org.jetbrains.likePrinter.components.lists.TypeParameterListComponent
import org.jetbrains.likePrinter.components.lists.ParameterListComponent
import org.jetbrains.likePrinter.components.lists.ResourceListComponent
import org.jetbrains.likePrinter.components.types.TypeParameterComponent
import org.jetbrains.likePrinter.components.variables.VariableComponent
import org.jetbrains.likePrinter.components.variables.ResourceVariableComponent
import org.jetbrains.likePrinter.components.variables.ParameterComponent
import com.intellij.psi.PsiWhiteSpace
import java.util.HashMap
import com.intellij.psi.PsiComment
import com.intellij.psi.PsiJavaToken
import org.jetbrains.likePrinter.components.expressions.PostfixComponent
import com.intellij.psi.PsiPostfixExpression
import com.intellij.psi.PsiExpressionStatement
import org.jetbrains.likePrinter.components.expressions.PrefixComponent
import com.intellij.psi.PsiPrefixExpression
import com.intellij.psi.PsiAssignmentExpression
import org.jetbrains.likePrinter.components.expressions.AssignmentComponent
import org.jetbrains.likePrinter.components.statements.ExpressionStatementComponent
import org.jetbrains.likePrinter.components.expressions.TypeCastComponent
import com.intellij.psi.PsiTypeCastExpression
import org.jetbrains.likePrinter.components.expressions.ConditionalComponent
import com.intellij.psi.PsiConditionalExpression
import org.jetbrains.likePrinter.components.expressions.QualifiedComponent
import com.intellij.psi.PsiInstanceOfExpression
import org.jetbrains.likePrinter.components.expressions.ArrayAccessComponent
import com.intellij.psi.PsiArrayAccessExpression
import org.jetbrains.likePrinter.components.expressions.BinaryComponent
import com.intellij.psi.PsiBinaryExpression
import org.jetbrains.likePrinter.components.expressions.ParenthesizedComponent
import com.intellij.psi.PsiParenthesizedExpression
import com.intellij.psi.PsiBreakStatement
import org.jetbrains.likePrinter.components.statements.BreakComponent
import org.jetbrains.likePrinter.components.statements.ContinueComponent
import com.intellij.psi.PsiContinueStatement
import org.jetbrains.likePrinter.components.statements.ReturnComponent
import com.intellij.psi.PsiReturnStatement
import com.intellij.psi.PsiAssertStatement
import org.jetbrains.likePrinter.components.statements.AssertComponent
import org.jetbrains.likePrinter.components.statements.ThrowComponent
import com.intellij.psi.PsiThrowStatement
import org.jetbrains.likePrinter.components.statements.LabeledComponent
import com.intellij.psi.PsiLabeledStatement
import org.jetbrains.likePrinter.components.expressions.PolyadicComponent
import com.intellij.psi.PsiPolyadicExpression
import com.intellij.psi.PsiExpressionList
import org.jetbrains.likePrinter.components.lists.ExpressionListComponent
import org.jetbrains.likePrinter.components.expressions.ReferenceComponent
import com.intellij.psi.PsiReferenceExpression
import org.jetbrains.likePrinter.components.expressions.MethodCallComponent
import com.intellij.psi.PsiMethodCallExpression
import com.intellij.psi.PsiArrayInitializerExpression
import org.jetbrains.likePrinter.components.lists.ArrayInitializerComponent
import com.intellij.psi.PsiDeclarationStatement
import org.jetbrains.likePrinter.components.lists.DeclarationComponent
import com.intellij.util.IncorrectOperationException
import org.jetbrains.likePrinter.components.expressions.NewComponent
import org.jetbrains.likePrinter.components.expressions.InstanceOfComponent
import com.intellij.psi.PsiQualifiedExpression
import org.jetbrains.likePrinter.components.AnnotationMethodComponent
import com.intellij.psi.PsiAnnotationMethod
import org.jetbrains.likePrinter.components.lists.AnnotationParameterListComponent
import com.intellij.psi.PsiAnnotationParameterList
import com.intellij.psi.PsiNameValuePair
import org.jetbrains.likePrinter.components.lists.NameValuePairComponent
import org.jetbrains.likePrinter.components.AnnotationComponent
import com.intellij.psi.PsiAnnotation
import com.intellij.psi.PsiArrayInitializerMemberValue
import org.jetbrains.likePrinter.components.lists.ArrayInitializerMemberValueComponent
import org.jetbrains.likePrinter.components.ModifierListComponent
import com.intellij.psi.PsiModifierList
import org.jetbrains.likePrinter.components.types.TypeElementComponent
import com.intellij.psi.PsiTypeElement
import org.jetbrains.likePrinter.components.expressions.ClassObjectAccessComponent
import com.intellij.psi.PsiClassObjectAccessExpression
import org.jetbrains.likePrinter.components.PackageComponent
import com.intellij.psi.PsiPackageStatement
import org.jetbrains.likePrinter.components.ImportStatementBaseComponent
import com.intellij.psi.PsiImportStatementBase
import org.jetbrains.likePrinter.components.lists.ImportListComponent
import com.intellij.psi.PsiImportList
import com.intellij.psi.PsiReferenceParameterList
import org.jetbrains.likePrinter.components.lists.ReferenceParameterListComponent
import org.jetbrains.likePrinter.components.JavaCodeReferenceComponent
import com.intellij.psi.PsiJavaCodeReferenceElement
import com.intellij.psi.PsiFileFactory
import org.jetbrains.likePrinter.components.JavaFileComponent
import com.intellij.ide.highlighter.JavaFileType
import com.intellij.codeInsight.documentation.DocumentationManager
import com.intellij.psi.PsiDocumentManager
import com.intellij.psi.PsiReference
import com.intellij.psi.PsiField
import org.jetbrains.likePrinter.components.statements.SwitchLabelComponent
import com.intellij.psi.PsiSwitchLabelStatement
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.CommentConnection
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import org.jetbrains.format.FormatSet.FormatSetType
import java.lang.management.ManagementFactory
import com.intellij.openapi.command.WriteCommandAction
import org.jetbrains.format.Format
import org.jetbrains.format.FormatSet
import org.jetbrains.format.SteppedFormatMap
import org.jetbrains.format.FormatMap3D_AF
import org.jetbrains.format.FormatMap3D
import org.jetbrains.format.FormatMap2D_LL
import org.jetbrains.format.FormatMap1D
import org.jetbrains.format.FormatList
import org.jetbrains.likePrinter.performUndoWrite

/**
 * User: anlun
 */

class Printer(
  templateFile: PsiJavaFile?
, private val settings: PrinterSettings
): Memoization(), CommentConnectionUtils {
    companion object {
        public fun create(templateFile: PsiJavaFile?, project: Project, width: Int): Printer =
                Printer(templateFile, PrinterSettings.createProjectSettings(width, project))
    }

    public fun hasToUseMultipleListElemVariants(): Boolean = settings.multipleListElemVariants
    public fun hasToUseMultipleExprStmtVariants(): Boolean = settings.multipleExprStmtVariants

    public fun setMultipleListElemVariantNeeds(f: Boolean) { settings.multipleListElemVariants = f }
    public fun setMultipleExprStmtVariantNeeds(f: Boolean) { settings.multipleExprStmtVariants = f }
    public fun setFormatSetType(f: FormatSetType) { settings.formatSetType = f }
    public fun setMaxWidth(f: Int) { settings.width = f }

    override public fun getMaxWidth(): Int = settings.width
    public fun    getProject(): Project   = settings.project
    public fun   getEmptySet(): FormatSet =
        when (settings.formatSetType) {
            FormatSetType.D1   -> FormatMap1D   (getMaxWidth())
            FormatSetType.D2   -> FormatMap2D_LL(getMaxWidth())
            FormatSetType.D3   -> FormatMap3D   (getMaxWidth())
            FormatSetType.D3AF -> FormatMap3D_AF(getMaxWidth())
            FormatSetType.List -> FormatList    (getMaxWidth())
            FormatSetType.SteppedD3AF -> SteppedFormatMap(FormatSet.stepInMap, getMaxWidth())

            else -> FormatMap3D(getMaxWidth())
        }
    override public fun getInitialSet(f: Format): FormatSet {
        val fs = getEmptySet()
        fs.add(f)
        return fs
    }

    //WARNING: must be declared before init!!!
    //COMPONENTS
    public val                      ifComponent:                      IfComponent =                      IfComponent(this)
    public val                   whileComponent:                   WhileComponent =                   WhileComponent(this)
    public val                 doWhileComponent:                 DoWhileComponent =                 DoWhileComponent(this)
    public val                     forComponent:                     ForComponent =                     ForComponent(this)
    public val                 foreachComponent:                 ForeachComponent =                 ForeachComponent(this)
    public val                  switchComponent:                  SwitchComponent =                  SwitchComponent(this)
    public val            synchronizedComponent:            SynchronizedComponent =            SynchronizedComponent(this)
    public val               codeBlockComponent:               CodeBlockComponent =               CodeBlockComponent(this)
    public val                     tryComponent:                     TryComponent =                     TryComponent(this)
    public val                  lambdaComponent:                  LambdaComponent =                  LambdaComponent(this)
    public val                 postfixComponent:                 PostfixComponent =                 PostfixComponent(this)
    public val                  prefixComponent:                  PrefixComponent =                  PrefixComponent(this)
    public val              assignmentComponent:              AssignmentComponent =              AssignmentComponent(this)
    public val                   classComponent:                   ClassComponent =                   ClassComponent(this)
    public val                typeCastComponent:                TypeCastComponent =                TypeCastComponent(this)
    public val             conditionalComponent:             ConditionalComponent =             ConditionalComponent(this)
    public val              instanceOfComponent:              InstanceOfComponent =              InstanceOfComponent(this)
    public val             arrayAccessComponent:             ArrayAccessComponent =             ArrayAccessComponent(this)
    public val             switchLabelComponent:             SwitchLabelComponent =             SwitchLabelComponent(this)
    public val        annotationMethodComponent:        AnnotationMethodComponent =        AnnotationMethodComponent(this)
    public val                  methodComponent:                  MethodComponent =                  MethodComponent(this)
    public val               qualifiedComponent:               QualifiedComponent =               QualifiedComponent(this)
    public val                  binaryComponent:                  BinaryComponent =                  BinaryComponent(this)
    public val                polyadicComponent:                PolyadicComponent =                PolyadicComponent(this)
    public val           parenthesizedComponent:           ParenthesizedComponent =           ParenthesizedComponent(this)
    public val                   breakComponent:                   BreakComponent =                   BreakComponent(this)
    public val                continueComponent:                ContinueComponent =                ContinueComponent(this)
    public val                  returnComponent:                  ReturnComponent =                  ReturnComponent(this)
    public val                  assertComponent:                  AssertComponent =                  AssertComponent(this)
    public val                   throwComponent:                   ThrowComponent =                   ThrowComponent(this)
    public val                 labeledComponent:                 LabeledComponent =                 LabeledComponent(this)
    public val          anonymousClassComponent:          AnonymousClassComponent =          AnonymousClassComponent(this)
    public val        classInitializerComponent:        ClassInitializerComponent =        ClassInitializerComponent(this)
    public val     expressionStatementComponent:     ExpressionStatementComponent =     ExpressionStatementComponent(this)
    public val enumConstantInitializerComponent: EnumConstantInitializerComponent = EnumConstantInitializerComponent(this)
    public val            enumConstantComponent:            EnumConstantComponent =            EnumConstantComponent(this)
    public val           referenceListComponent:           ReferenceListComponent =           ReferenceListComponent(this)
    public val       typeParameterListComponent:       TypeParameterListComponent =       TypeParameterListComponent(this)
    public val           parameterListComponent:           ParameterListComponent =           ParameterListComponent(this)
    public val            resourceListComponent:            ResourceListComponent =            ResourceListComponent(this)
    public val          expressionListComponent:          ExpressionListComponent =          ExpressionListComponent(this)
    public val annotationParameterListComponent: AnnotationParameterListComponent = AnnotationParameterListComponent(this)
    public val  referenceParameterListComponent:  ReferenceParameterListComponent =  ReferenceParameterListComponent(this)
    public val            modifierListComponent:            ModifierListComponent =            ModifierListComponent(this)
    public val           typeParameterComponent:           TypeParameterComponent =           TypeParameterComponent(this)
    public val               parameterComponent:               ParameterComponent =               ParameterComponent(this)
    public val                variableComponent:                VariableComponent =                VariableComponent(this)
    public val        resourceVariableComponent:        ResourceVariableComponent =        ResourceVariableComponent(this)
    public val             typeElementComponent:             TypeElementComponent =             TypeElementComponent(this)
    public val     referenceExpressionComponent:               ReferenceComponent =               ReferenceComponent(this)
    public val           newExpressionComponent:                     NewComponent =                     NewComponent(this)
    public val        arrayInitializerComponent:        ArrayInitializerComponent =        ArrayInitializerComponent(this)
    public val             declarationComponent:             DeclarationComponent =             DeclarationComponent(this)
    public val              methodCallComponent:              MethodCallComponent =              MethodCallComponent(this)
    public val           nameValuePairComponent:           NameValuePairComponent =           NameValuePairComponent(this)
    public val              annotationComponent:              AnnotationComponent =              AnnotationComponent(this)
    public val       classObjectAccessComponent:       ClassObjectAccessComponent =       ClassObjectAccessComponent(this)
    public val                 packageComponent:                 PackageComponent =                 PackageComponent(this)
    public val     importStatementBaseComponent:     ImportStatementBaseComponent =     ImportStatementBaseComponent(this)
    public val              importListComponent:              ImportListComponent =              ImportListComponent(this)
    public val       javaCodeReferenceComponent:       JavaCodeReferenceComponent =       JavaCodeReferenceComponent(this)
    public val                javaFileComponent:                JavaFileComponent =                JavaFileComponent(this)

    public val arrayInitializerMemberValueComponent: ArrayInitializerMemberValueComponent = ArrayInitializerMemberValueComponent(this)

    public fun reprint(javaFile: PsiJavaFile) { reprintElementWithChildren(javaFile) }

    init {
        if (templateFile != null) {
            fillTemplateLists(templateFile)
        }
    }

    /// public only for testing purposes!!!
    public fun reprintElementWithChildren(psiElement: PsiElement) {
        reprintElementWithChildren_AllMeaningful(psiElement) // variant for partial template
//        reprintElementWithChildren_OnlyPsiJavaFile(psiElement) // variant for situations with full template
    }

    private fun reprintElementWithChildren_OnlyPsiJavaFile(psiElement: PsiElement) {
        walker(psiElement) { p -> if (p is PsiJavaFile) applyTmplt(p) }
    }

    private fun reprintElementWithChildren_AllMeaningful(psiElement: PsiElement) {
        walker(psiElement) { p ->
            when (p) {
                  is PsiTypeParameter
                , is PsiExpressionList                   //Only inside Call printing!!!
                , is PsiEnumConstantInitializer -> 5 + 5 //Only inside Enum printing!!!

                  is PsiIfStatement
                , is PsiWhileStatement
                , is PsiDoWhileStatement
                , is PsiForStatement
                , is PsiForeachStatement
                , is PsiSwitchStatement
                , is PsiTryStatement
                , is PsiSynchronizedStatement
                , is PsiMethod

                , is PsiNewExpression
                , is PsiLambdaExpression
                , is PsiPostfixExpression
                , is PsiPrefixExpression
                , is PsiAssignmentExpression
                , is PsiExpressionStatement
                , is PsiInstanceOfExpression
                , is PsiTypeCastExpression
                , is PsiArrayAccessExpression

                , is PsiPolyadicExpression
//                , is PsiArrayInitializerExpression //Prints not in template

//                , is PsiParameter
                , is PsiVariable
                , is PsiDeclarationStatement

                , is PsiReferenceExpression
                , is PsiMethodCallExpression

                , is PsiConditionalExpression
                , is PsiParenthesizedExpression
                , is PsiAnonymousClass
                , is PsiBreakStatement
                , is PsiContinueStatement
                , is PsiReturnStatement
                , is PsiAssertStatement
                , is PsiThrowStatement
                , is PsiLabeledStatement
                , is PsiAnnotation
                , is PsiClass

                , is PsiJavaFile  -> applyTmplt(p)

//                is PsiReferenceList        -> applyTmplt(p)
//                is PsiParameterList        -> applyTmplt(p)
//                is PsiClassInitializer     -> applyTmplt(p)
//                is PsiCodeBlock            -> applyTmplt(p)
                else -> 5 + 5
            }
        }
    }

    public fun getVariants(p: PsiElement, context: VariantConstructionContext = defaultContext()): FormatSet {
        val pCommentContext = getCommentContext(p)
        val widthToSuit = context.widthToSuit
        val variantConstructionContext = VariantConstructionContext(pCommentContext, widthToSuit)

        val mv = getMemoizedVariants(p)
        if (mv != null) { return surroundVariantsByAttachedComments(p, mv, context) }

        val resultWithoutOuterContextComments: FormatSet
        val templateVariant = getTemplateVariants(p, variantConstructionContext)
        if (templateVariant.isNotEmpty()) {
            resultWithoutOuterContextComments = surroundVariantsByAttachedComments(
                      p, templateVariant, variantConstructionContext
            )

            addToCache(p, resultWithoutOuterContextComments)
        } else {
            val s = p.getText() ?: ""
            if (s.contains(" ")) { log.add(s) }
            resultWithoutOuterContextComments = getVariantsByText(p)

            //TODO: For test purposes!!!
            addToCache(p, resultWithoutOuterContextComments)
        }

        val variants = surroundVariantsByAttachedComments(p, resultWithoutOuterContextComments, context)
        return variants
    }

    override public fun getVariantsByText(p: PsiElement): FormatSet {
        val offsetInStartLine = p.getOffsetInStartLine()
        val normalizedFillConstant = Math.max(p.getFillConstant(), 0)
        return getInitialSet(Format.text(p.getText(), offsetInStartLine + normalizedFillConstant))
    }

    private fun getTemplateVariants(p: PsiElement, context: VariantConstructionContext): FormatSet {
        val variants: FormatSet =
            when(p) {
                is PsiIfStatement                ->                           ifComponent.getVariants(p, context)
                is PsiWhileStatement             ->                        whileComponent.getVariants(p, context)
                is PsiDoWhileStatement           ->                      doWhileComponent.getVariants(p, context)
                is PsiForStatement               ->                          forComponent.getVariants(p, context)
                is PsiForeachStatement           ->                      foreachComponent.getVariants(p, context)
                is PsiSwitchStatement            ->                       switchComponent.getVariants(p, context)
                is PsiSynchronizedStatement      ->                 synchronizedComponent.getVariants(p, context)
                is PsiCodeBlock                  ->                    codeBlockComponent.getVariants(p, context)
                is PsiTryStatement               ->                          tryComponent.getVariants(p, context)
                is PsiNewExpression              ->                newExpressionComponent.getVariants(p, context)
                is PsiLambdaExpression           ->                       lambdaComponent.getVariants(p, context)
                is PsiPostfixExpression          ->                      postfixComponent.getVariants(p, context)
                is PsiPrefixExpression           ->                       prefixComponent.getVariants(p, context)
                is PsiAssignmentExpression       ->                   assignmentComponent.getVariants(p, context)
                is PsiClassInitializer           ->             classInitializerComponent.getVariants(p, context)
                is PsiTypeCastExpression         ->                     typeCastComponent.getVariants(p, context)
                is PsiConditionalExpression      ->                  conditionalComponent.getVariants(p, context)
                is PsiInstanceOfExpression       ->                   instanceOfComponent.getVariants(p, context)
                is PsiArrayAccessExpression      ->                  arrayAccessComponent.getVariants(p, context)
                is PsiSwitchLabelStatement       ->                  switchLabelComponent.getVariants(p, context)

                is PsiAnnotationMethod           ->             annotationMethodComponent.getVariants(p, context)
                is PsiMethod                     ->                       methodComponent.getVariants(p, context)

                is PsiQualifiedExpression        ->                    qualifiedComponent.getVariants(p, context)
                is PsiPolyadicExpression         ->                     polyadicComponent.getVariants(p, context)

                is PsiReferenceExpression        ->          referenceExpressionComponent.getVariants(p, context)
                is PsiMethodCallExpression       ->                   methodCallComponent.getVariants(p, context)

                is PsiArrayInitializerExpression ->             arrayInitializerComponent.getVariants(p, context)
                is PsiDeclarationStatement       ->                  declarationComponent.getVariants(p, context)

                is PsiParenthesizedExpression    ->                parenthesizedComponent.getVariants(p, context)
                is PsiBreakStatement             ->                        breakComponent.getVariants(p, context)
                is PsiContinueStatement          ->                     continueComponent.getVariants(p, context)
                is PsiReturnStatement            ->                       returnComponent.getVariants(p, context)
                is PsiAssertStatement            ->                       assertComponent.getVariants(p, context)
                is PsiThrowStatement             ->                        throwComponent.getVariants(p, context)
                is PsiLabeledStatement           ->                      labeledComponent.getVariants(p, context)

                is PsiResourceList               ->                 resourceListComponent.getVariants(p, context)
                is PsiReferenceList              ->                referenceListComponent.getVariants(p, context)
                is PsiParameterList              ->                parameterListComponent.getVariants(p, context)
                is PsiTypeParameterList          ->            typeParameterListComponent.getVariants(p, context)
                is PsiExpressionList             ->               expressionListComponent.getVariants(p, context)
                is PsiAnnotationParameterList    ->      annotationParameterListComponent.getVariants(p, context)
                is PsiReferenceParameterList     ->       referenceParameterListComponent.getVariants(p, context)

                is PsiTypeParameter              ->                typeParameterComponent.getVariants(p, context)
                is PsiTypeElement                ->                  typeElementComponent.getVariants(p, context)

                is PsiAnonymousClass             ->               anonymousClassComponent.getVariants(p, context)
                is PsiClass                      ->                        classComponent.getVariants(p, context)

                is PsiParameter                  ->                    parameterComponent.getVariants(p, context)
                is PsiEnumConstant               ->                 enumConstantComponent.getVariants(p, context)
                is PsiResourceVariable           ->             resourceVariableComponent.getVariants(p, context)
                is PsiVariable                   ->                     variableComponent.getVariants(p, context)

                is PsiExpressionStatement        ->          expressionStatementComponent.getVariants(p               , context)
                is PsiBlockStatement             ->                    codeBlockComponent.getVariants(p.getCodeBlock(), context)

                is PsiNameValuePair              ->                nameValuePairComponent.getVariants(p, context)
                is PsiAnnotation                 ->                   annotationComponent.getVariants(p, context)

                is PsiArrayInitializerMemberValue -> arrayInitializerMemberValueComponent.getVariants(p, context)
                is PsiModifierList                ->                modifierListComponent.getVariants(p, context)
                is PsiClassObjectAccessExpression ->           classObjectAccessComponent.getVariants(p, context)

                is PsiPackageStatement            ->                     packageComponent.getVariants(p, context)
                is PsiJavaCodeReferenceElement    ->           javaCodeReferenceComponent.getVariants(p, context)

                is PsiImportStatementBase         ->         importStatementBaseComponent.getVariants(p, context)
                is PsiImportList                  ->                  importListComponent.getVariants(p, context)

                is PsiJavaFile                    ->                    javaFileComponent.getVariants(p, context)

                //Just cut from text
                else -> {
//                    println("AAA: ${p.getClass()}")
                    getEmptySet()
                }
            }

        return variants
    }

    public  fun areTemplatesFilled(): Boolean = areTemplatesFilled
    private var areTemplatesFilled  : Boolean = false

    public fun fillTemplateLists(templateFile: PsiJavaFile) {
        areTemplatesFilled = true
        walker(templateFile, { p: PsiElement ->
            when (p) {
                is PsiIfStatement                 ->                          ifComponent.getAndSaveTemplate(p)
                is PsiWhileStatement              ->                       whileComponent.getAndSaveTemplate(p)
                is PsiDoWhileStatement            ->                     doWhileComponent.getAndSaveTemplate(p)
                is PsiForStatement                ->                         forComponent.getAndSaveTemplate(p)
                is PsiForeachStatement            ->                     foreachComponent.getAndSaveTemplate(p)
                is PsiSwitchStatement             ->                      switchComponent.getAndSaveTemplate(p)
                is PsiSynchronizedStatement       ->                synchronizedComponent.getAndSaveTemplate(p)
                is PsiAnnotationMethod            ->            annotationMethodComponent.getAndSaveTemplate(p)
                is PsiMethod                      ->                      methodComponent.getAndSaveTemplate(p)
                is PsiCodeBlock                   ->                   codeBlockComponent.getAndSaveTemplate(p)
                is PsiTryStatement                ->                         tryComponent.getAndSaveTemplate(p)
                is PsiNewExpression               ->               newExpressionComponent.getAndSaveTemplate(p)
                is PsiLambdaExpression            ->                      lambdaComponent.getAndSaveTemplate(p)
                is PsiPostfixExpression           ->                     postfixComponent.getAndSaveTemplate(p)
                is PsiPrefixExpression            ->                      prefixComponent.getAndSaveTemplate(p)
                is PsiAssignmentExpression        ->                  assignmentComponent.getAndSaveTemplate(p)
                is PsiClassInitializer            ->            classInitializerComponent.getAndSaveTemplate(p)
                is PsiReferenceList               ->               referenceListComponent.getAndSaveTemplate(p)
                is PsiParameterList               ->               parameterListComponent.getAndSaveTemplate(p)
                is PsiTypeParameterList           ->           typeParameterListComponent.getAndSaveTemplate(p)
                is PsiResourceList                ->                resourceListComponent.getAndSaveTemplate(p)
                is PsiExpressionList              ->              expressionListComponent.getAndSaveTemplate(p)
                is PsiAnnotationParameterList     ->     annotationParameterListComponent.getAndSaveTemplate(p)
                is PsiReferenceParameterList      ->      referenceParameterListComponent.getAndSaveTemplate(p)
                is PsiTypeParameter               ->               typeParameterComponent.getAndSaveTemplate(p)
                is PsiEnumConstantInitializer     ->     enumConstantInitializerComponent.getAndSaveTemplate(p)
                is PsiAnonymousClass              ->              anonymousClassComponent.getAndSaveTemplate(p)
                is PsiClass                       ->                       classComponent.getAndSaveTemplate(p)
                is PsiTypeCastExpression          ->                    typeCastComponent.getAndSaveTemplate(p)
                is PsiParameter                   ->                   parameterComponent.getAndSaveTemplate(p)
                is PsiEnumConstant                ->                enumConstantComponent.getAndSaveTemplate(p)
                is PsiResourceVariable            ->            resourceVariableComponent.getAndSaveTemplate(p)
                is PsiVariable                    ->                    variableComponent.getAndSaveTemplate(p)
                is PsiExpressionStatement         ->         expressionStatementComponent.getAndSaveTemplate(p)
                is PsiConditionalExpression       ->                 conditionalComponent.getAndSaveTemplate(p)
                is PsiInstanceOfExpression        ->                  instanceOfComponent.getAndSaveTemplate(p)
                is PsiArrayAccessExpression       ->                 arrayAccessComponent.getAndSaveTemplate(p)
                is PsiSwitchLabelStatement        ->                 switchLabelComponent.getAndSaveTemplate(p)
                is PsiBinaryExpression            ->                      binaryComponent.getAndSaveTemplate(p)
                is PsiParenthesizedExpression     ->               parenthesizedComponent.getAndSaveTemplate(p)
                is PsiBreakStatement              ->                       breakComponent.getAndSaveTemplate(p)
                is PsiContinueStatement           ->                    continueComponent.getAndSaveTemplate(p)
                is PsiReturnStatement             ->                      returnComponent.getAndSaveTemplate(p)
                is PsiAssertStatement             ->                      assertComponent.getAndSaveTemplate(p)
                is PsiThrowStatement              ->                       throwComponent.getAndSaveTemplate(p)
                is PsiLabeledStatement            ->                     labeledComponent.getAndSaveTemplate(p)
                is PsiReferenceExpression         ->         referenceExpressionComponent.getAndSaveTemplate(p)
                is PsiMethodCallExpression        ->                  methodCallComponent.getAndSaveTemplate(p)
                is PsiArrayInitializerExpression  ->            arrayInitializerComponent.getAndSaveTemplate(p)
                is PsiDeclarationStatement        ->                 declarationComponent.getAndSaveTemplate(p)
                is PsiNameValuePair               ->               nameValuePairComponent.getAndSaveTemplate(p)
                is PsiAnnotation                  ->                  annotationComponent.getAndSaveTemplate(p)
                is PsiArrayInitializerMemberValue -> arrayInitializerMemberValueComponent.getAndSaveTemplate(p)
                is PsiModifierList                ->                modifierListComponent.getAndSaveTemplate(p)
                is PsiTypeElement                 ->                 typeElementComponent.getAndSaveTemplate(p)
                is PsiClassObjectAccessExpression ->           classObjectAccessComponent.getAndSaveTemplate(p)
                is PsiPackageStatement            ->                     packageComponent.getAndSaveTemplate(p)
                is PsiImportStatementBase         ->         importStatementBaseComponent.getAndSaveTemplate(p)
                is PsiJavaCodeReferenceElement    ->           javaCodeReferenceComponent.getAndSaveTemplate(p)

                else -> 5 + 5
            }
        })
    }

    private fun createElementFromText(p: PsiElement, text: String): PsiElement? {
        val factory = JavaPsiFacade.getElementFactory(getProject()) ?: return null
        when (p) {
            is PsiMethod       -> return factory.      createMethodFromText(text, null)
            is PsiEnumConstant -> return factory.createEnumConstantFromText(text, null)
            is PsiAnonymousClass -> {
                val exp = factory.createExpressionFromText("new\n$text", null)
                if (exp !is PsiNewExpression) { return null }
                val newAnonymousClass = exp.getAnonymousClass()
                return newAnonymousClass
            }
            is PsiClass -> {
                val dummyClass = factory.createClassFromText(text, null)
                val allInnerClasses = dummyClass.getAllInnerClasses()
                val newClass = allInnerClasses[0]
                return newClass
            }
            is PsiParameter  -> return factory. createParameterFromText(text, null)
            is PsiAnnotation -> return factory.createAnnotationFromText(text, null)
//            is PsiJavaCodeReferenceElement -> return factory?.createReferenceFromText(text, null)
            is PsiField            -> return factory.        createFieldFromText(text, null)
            is PsiTypeElement      -> return factory.  createTypeElementFromText(text, null)
            is PsiCodeBlock        -> return factory.    createCodeBlockFromText(text, null)
            is PsiResourceVariable -> return factory.     createResourceFromText(text, null)
            is PsiTypeParameter    -> return factory.createTypeParameterFromText(text, null)


            is PsiExpression -> return factory.createExpressionFromText(text, null)
            else -> return factory.createStatementFromText(text, null)

            //In case of (PsiClassInitializer, PsiParameter, Psi*List) it need separate creator, but now this situation is impossible
        }
    }

    private fun applyTmplt(p: PsiElement) {
        val formatSet = getVariants(p)

        val threadMXBean = ManagementFactory.getThreadMXBean()!!
        val startTime = threadMXBean.getCurrentThreadCpuTime()
        val chosenFormat = formatSet.head() ?: return

        fun replaceElement(newElement: PsiElement) {
            getProject().performUndoWrite { p.replace(newElement) }
        }

        val startLineOffset = p.getOffsetInStartLine()
        val newElementText = chosenFormat.toText(startLineOffset, "")

        if (p is PsiJavaFile) {
            val document = PsiDocumentManager.getInstance(getProject())?.getDocument(p)
            val oldDocSize = document?.getText()?.length()
            if (document == null || oldDocSize == null) { return }
            getProject().performUndoWrite {
                document.replaceString(0, oldDocSize, newElementText)
            }
            return
        }

        val statement: PsiElement
        try {
            val createdStatement = createElementFromText(p, newElementText) ?: return
            statement = createdStatement
        } catch (e: IncorrectOperationException) { return }

        if (p is PsiCodeBlock && statement is PsiBlockStatement) {
            renewCache(p, statement)
            replaceElement(statement.getCodeBlock())
            return
        }
        renewCache(p, statement)
        replaceElement(statement)

        val endTime = threadMXBean.getCurrentThreadCpuTime()
        replaceTime += endTime - startTime
    }
}

