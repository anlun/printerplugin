package org.jetbrains.likePrinter.ui;

import com.intellij.openapi.fileChooser.FileChooser;
import com.intellij.openapi.fileChooser.FileChooserDescriptor;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.TextFieldWithBrowseButton;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiFileFactory;
import com.intellij.util.Consumer;
import org.jetbrains.likePrinter.PrinterOwner;
import org.jetbrains.likePrinter.printer.Printer;
import org.jetbrains.likePrinter.printer.PrinterPackage;

import javax.swing.*;
import java.awt.event.*;
import java.util.List;

public class PrinterInitDialog extends JDialog {
  private JPanel contentPane;
  private JButton buttonOK;
  private JButton buttonCancel;
  private JSpinner textWidthSpinner;
  private JSpinner tabSizeSpinner;
  private TextFieldWithBrowseButton templateFileChooserButton;
  private PrinterOwner printerOwner;
  private Project project;

  private PsiFileFactory factory;
  private List<VirtualFile> chosenFiles;

  public PrinterInitDialog(PrinterOwner printerOwner, Project project, PsiFileFactory factory) {
    this.printerOwner = printerOwner;
    this.project      = project;
    this.factory      = factory;

    setContentPane(contentPane);
    setModal(true);
    getRootPane().setDefaultButton(buttonOK);
    setLocationRelativeTo(getParent());
    setResizable(false);
    this.setTitle("Choose print settings...");

    textWidthSpinner   .setModel(new SpinnerNumberModel(100, 10, 250, 1));
    tabSizeSpinner     .setModel(new SpinnerNumberModel(  4,  0,  10, 1));

    templateFileChooserButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        FileChooser.chooseFiles(
            new FileChooserDescriptor(true, true, false, false, false, true)
            , null
            , null
            , new Consumer<List<VirtualFile>>() {
          @Override
          public void consume(List<VirtualFile> virtualFiles) {
            chosenFiles = virtualFiles;
          }
        });
      }
    });

    buttonOK.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onOK();
      }
    });

    buttonCancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onCancel();
      }
    });

// call onCancel() when cross is clicked
    setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e) {
        onCancel();
      }
    });

// call onCancel() on ESCAPE
    contentPane.registerKeyboardAction(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onCancel();
      }
    }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
  }

  private void onOK() {
    if (chosenFiles == null || chosenFiles.isEmpty()) {
      dispose();
      return;
    }

    Integer maxWidth = (Integer) textWidthSpinner.getValue();
    Printer printer = Printer.Companion.create(null, project, maxWidth);

    for (VirtualFile file : chosenFiles) {
      PrinterPackage.fillPrinterTemplatesByFile(file, printer, factory, (Integer) tabSizeSpinner.getValue());
    }

    printerOwner.setPrinter(printer);

    dispose();
  }

  private void onCancel() { dispose(); }
  private void createUIComponents() {}
}
