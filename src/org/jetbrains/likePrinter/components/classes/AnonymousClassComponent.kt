package org.jetbrains.likePrinter.components.classes

import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import org.jetbrains.likePrinter.components.ModifierListOwner
import org.jetbrains.likePrinter.components.NameOwner
import org.jetbrains.likePrinter.components.TypeParameterListOwner
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiTemplateGen
import com.intellij.psi.PsiAnonymousClass
import com.intellij.psi.PsiElementFactory
import com.intellij.psi.PsiNewExpression
import com.intellij.psi.PsiEnumConstantInitializer
import java.util.HashMap
import org.jetbrains.likePrinter.util.psiElement.getCorrectTextOffset
import org.jetbrains.likePrinter.util.psiElement.hasElement
import org.jetbrains.likePrinter.util.psiElement.toSmartInsertPlace
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext

import java.util.HashSet
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public class AnonymousClassComponent(
        printer: Printer
): PsiElementComponent   <PsiAnonymousClass, SmartInsertPlace, PsiTemplateGen<PsiAnonymousClass, SmartInsertPlace>>(printer)
 , ModifierListOwner     <PsiAnonymousClass, SmartInsertPlace, PsiTemplateGen<PsiAnonymousClass, SmartInsertPlace>>
 , TypeParameterListOwner<PsiAnonymousClass, SmartInsertPlace, PsiTemplateGen<PsiAnonymousClass, SmartInsertPlace>>
 , ClassBodyOwner        <PsiAnonymousClass, SmartInsertPlace, PsiTemplateGen<PsiAnonymousClass, SmartInsertPlace>>
 , ArgumentListOwner     <PsiAnonymousClass, SmartInsertPlace, PsiTemplateGen<PsiAnonymousClass, SmartInsertPlace>> {

    final val BASE_CLASS_REFERENCE_TAG: String
        get() = "base class reference"

    override protected fun getNewElement(text: String, elementFactory: PsiElementFactory): PsiAnonymousClass? {
        val expressionText = "new\n$text"
        val newExpression = elementFactory.createExpressionFromText(expressionText, null)
        if (newExpression !is PsiNewExpression) { return null }
        val newP = newExpression.getAnonymousClass()
        return newP
    }

    override public fun getTemplateFromElement(newP: PsiAnonymousClass): PsiTemplateGen<PsiAnonymousClass, SmartInsertPlace>? {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()
        val negShift = -newP.getCorrectTextOffset()

        addArgumentListToInsertPlaceMap(newP, insertPlaceMap, negShift)
        addClassBodyToInsertPlaceMap   (newP, insertPlaceMap)

        val baseClassReference = newP.getBaseClassReference()
        insertPlaceMap.put(BASE_CLASS_REFERENCE_TAG, baseClassReference.toSmartInsertPlace().shiftRight(negShift))

        val contentRelation = getContentRelation(newP.getText() ?: "", insertPlaceMap)
        return PsiTemplateGen(newP, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    override protected fun prepareSubtreeVariants(p: PsiAnonymousClass, context: VariantConstructionContext): Map<String, FormatSet> {
        val variants = HashMap<String, FormatSet>()

        prepareModifierListVariants     (p, variants, context)
        prepareTypeParameterListVariants(p, variants, context)
        prepareArgumentListVariants     (p, variants, context)

        val baseClassReferenceVariants = getBaseClassVariants(p, context)
        if (baseClassReferenceVariants.isNotEmpty()) {
            variants.put(BASE_CLASS_REFERENCE_TAG, baseClassReferenceVariants)
        }

        prepareBodyVariants(p, variants, context)

        return variants
    }

    private fun getBaseClassVariants(p: PsiAnonymousClass, context: VariantConstructionContext): FormatSet {
        val baseClassReference = p.getBaseClassReference()
        return printer.getVariants(baseClassReference, context)
    }

    override protected fun getTags(p: PsiAnonymousClass): Set<String> {
        val set = HashSet<String>()

        if (hasElement(p.getModifierList())) { set.add(MODIFIER_LIST_TAG) }
        set.add(BASE_CLASS_REFERENCE_TAG)
        if (hasElement(p.getTypeParameterList())) { set.add(TYPE_PARAMETER_LIST_TAG) }
        if (hasElement(p.     getArgumentList())) { set.add(      ARGUMENT_LIST_TAG) }
        set.add(CLASS_BODY_TAG)

        return set
    }
    override protected fun isTemplateSuitable(
            p: PsiAnonymousClass, tmplt: PsiTemplateGen<PsiAnonymousClass, SmartInsertPlace>
    ): Boolean {
        if (p is PsiEnumConstantInitializer) { return false }
        return isTemplateSuitable_ArgumentListOwner(p, tmplt)
    }
}