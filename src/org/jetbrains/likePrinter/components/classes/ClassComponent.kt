package org.jetbrains.likePrinter.components.classes

import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import com.intellij.psi.PsiClass
import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.components.ModifierListOwner
import com.intellij.psi.PsiElementFactory
import java.util.HashMap
import org.jetbrains.likePrinter.templateBase.template.Template
import org.jetbrains.likePrinter.util.psiElement.hasElement
import org.jetbrains.likePrinter.util.psiElement.toSmartInsertPlace
import org.jetbrains.likePrinter.components.NameOwner
import org.jetbrains.likePrinter.components.TypeParameterListOwner
import com.intellij.psi.PsiTypeParameter
import com.intellij.openapi.util.TextRange
import org.jetbrains.likePrinter.util.base.InsertPlace
import org.jetbrains.likePrinter.util.psiElement.getNotNullTextRange
import com.intellij.psi.PsiWhiteSpace
import org.jetbrains.likePrinter.util.psiElement.getTextRange
import org.jetbrains.likePrinter.util.psiElement.*
import org.jetbrains.likePrinter.templateBase.template.PsiTemplateGen
import org.jetbrains.format.Format
import com.intellij.psi.PsiEnumConstant

import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import java.util.HashSet
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public class ClassComponent(
        printer: Printer
): PsiElementComponent   <PsiClass, SmartInsertPlace, PsiTemplateGen<PsiClass, SmartInsertPlace>>(printer)
 , ModifierListOwner     <PsiClass, SmartInsertPlace, PsiTemplateGen<PsiClass, SmartInsertPlace>>
 , NameOwner             <PsiClass, SmartInsertPlace, PsiTemplateGen<PsiClass, SmartInsertPlace>>
 , TypeParameterListOwner<PsiClass, SmartInsertPlace, PsiTemplateGen<PsiClass, SmartInsertPlace>>
 , ClassBodyOwner        <PsiClass, SmartInsertPlace, PsiTemplateGen<PsiClass, SmartInsertPlace>> {

    override protected fun getNewElement(text: String, elementFactory: PsiElementFactory): PsiClass? {
        val newDummyClass = elementFactory.createClassFromText(text, null)
        if (newDummyClass !is PsiClass) { return null }
        val newP = newDummyClass.getAllInnerClasses()[0]
        return newP
    }

    override public fun getTemplateFromElement(newP: PsiClass): PsiTemplateGen<PsiClass, SmartInsertPlace>? {
        if (newP.getName().equals("_Dummy_")) { return null }
        if (newP is PsiTypeParameter)         { return null }

        val insertPlaceMap = HashMap<String, SmartInsertPlace>()

        val negShift = -newP.getCorrectTextOffset()

        addClassBodyToInsertPlaceMap        (newP, insertPlaceMap)
        addModifierListToInsertPlaceMap     (newP, insertPlaceMap, negShift)
        addNameToInsertPlaceMap             (newP, insertPlaceMap, negShift)
        addImplementsListToInsertPlaceMap   (newP, insertPlaceMap, negShift)
        addExtendsListToInsertPlaceMap      (newP, insertPlaceMap, negShift)
        addTypeParameterListToInsertPlaceMap(newP, insertPlaceMap, negShift)

        val contentRelation = getContentRelation(newP.getText() ?: "", insertPlaceMap)
        return PsiTemplateGen(newP, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    private fun PsiClass.hasExtendsList      (): Boolean = hasElement(getExtendsList   ())
    private fun PsiClass.hasImplementsList   (): Boolean = hasElement(getImplementsList())


    final val EXTENDS_LIST_TAG: String
        get() = "extends list"

    private fun addExtendsListToInsertPlaceMap(
            p: PsiClass, insertPlaceMap: MutableMap<String, SmartInsertPlace>, delta: Int = 0
    ) {
        val list = p.getExtendsList()
        if (list != null && hasElement(list)) {
            insertPlaceMap.put(EXTENDS_LIST_TAG, list.toSmartInsertPlace().shiftRight(delta))
        }
    }

    protected fun prepareExtendsListVariants(
              p: PsiClass
            , variants: MutableMap<String, FormatSet>
            , context: VariantConstructionContext
    ) {
        val listVariants = getExtendsListVariants(p, context)
        if (listVariants.isEmpty()) { return }
        variants.put(EXTENDS_LIST_TAG, listVariants)
    }

    private fun getExtendsListVariants(
              p: PsiClass
            , context: VariantConstructionContext
    ): FormatSet {
        val list = p.getExtendsList()
        if (list == null || !hasElement(list)) { return printer.getEmptySet() }
        return printer.getVariants(list, context)
    }

    final val IMPLEMENTS_LIST_TAG: String
        get() = "implements list"

    private fun addImplementsListToInsertPlaceMap(
            p: PsiClass, insertPlaceMap: MutableMap<String, SmartInsertPlace>, delta: Int = 0
    ) {
        val list = p.getImplementsList()
        if (list != null && hasElement(list)) {
            insertPlaceMap.put(IMPLEMENTS_LIST_TAG, list.toSmartInsertPlace().shiftRight(delta))
        }
    }

    protected fun prepareImplementsListVariants(
              p: PsiClass
            , variants: MutableMap<String, FormatSet>
            , context: VariantConstructionContext
    ) {
        val listVariants = getImplementsListVariants(p, context)
        if (listVariants.isEmpty()) { return }
        variants.put(IMPLEMENTS_LIST_TAG, listVariants)
    }

    private fun getImplementsListVariants(
              p: PsiClass
            , context: VariantConstructionContext
    ): FormatSet {
        val list = p.getImplementsList()
        if (list == null || !hasElement(list)) { return printer.getEmptySet() }
        return printer.getVariants(list, context)
    }

    override protected fun prepareSubtreeVariants(
              p: PsiClass
            , context: VariantConstructionContext
    ): Map<String, FormatSet> {
        val variants = HashMap<String, FormatSet>()

        prepareModifierListVariants     (p, variants, context)
        prepareNameVariants             (p, variants, context)
        prepareTypeParameterListVariants(p, variants, context)
        prepareExtendsListVariants      (p, variants, context)
        prepareImplementsListVariants   (p, variants, context)

        prepareBodyVariants(p, variants, context)

        return variants
    }

    override protected fun getTags(p: PsiClass): Set<String> {
        val set = HashSet<String>()

        if (hasElement(p.getModifierList())) { set.add(MODIFIER_LIST_TAG) }
        set.add(NAME_TAG)
        if (hasElement(p.getTypeParameterList())) { set.add(TYPE_PARAMETER_LIST_TAG) }
        if (hasElement(p.      getExtendsList())) { set.add(       EXTENDS_LIST_TAG) }
        if (hasElement(p.   getImplementsList())) { set.add(    IMPLEMENTS_LIST_TAG) }
        if (p.getBodyRange() != null)             { set.add(         CLASS_BODY_TAG) }

        return set
    }

    override protected fun isTemplateSuitable(p: PsiClass, tmplt: PsiTemplateGen<PsiClass, SmartInsertPlace>): Boolean {
        if (!isTemplateSuitable_ModifierListOwner     (p, tmplt)) { return false }
        if (!isTemplateSuitable_TypeParameterListOwner(p, tmplt)) { return false }

        if (p.hasExtendsList   () != (tmplt.insertPlaceMap.get(   EXTENDS_LIST_TAG) != null)) { return false }
        if (p.hasImplementsList() != (tmplt.insertPlaceMap.get(IMPLEMENTS_LIST_TAG) != null)) { return false }

        if (p.isEnum          () != tmplt.psi.isEnum          ()) { return false }
        if (p.isInterface     () != tmplt.psi.isInterface     ()) { return false }
        if (p.isAnnotationType() != tmplt.psi.isAnnotationType()) { return false }

        return true
    }
}