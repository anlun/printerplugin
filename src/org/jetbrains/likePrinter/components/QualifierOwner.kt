package org.jetbrains.likePrinter.components

import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.Template
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiExpression
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext

import org.jetbrains.likePrinter.util.psiElement.toSmartInsertPlace
import com.intellij.psi.PsiReferenceExpression
import com.intellij.psi.PsiNewExpression
import com.intellij.psi.PsiJavaCodeReferenceElement
import org.jetbrains.format.FormatSet


/**
 * User: anlun
 */
public interface QualifierOwner<ET: PsiElement, IPT: SmartInsertPlace, T: Template<IPT>>: PsiElementComponent<ET, IPT, T> {
    final val QUALIFIER_TAG: String
        get() = "qualifier"

    protected fun addQualifierToInsertPlaceMap(
              p: ET
            , insertPlaceMap: MutableMap<String, SmartInsertPlace>
            , delta: Int
    ): Boolean {
        val qualifierSIP = getQualifier(p)?.toSmartInsertPlace()
        if (qualifierSIP == null) { return false }
        insertPlaceMap.put(QUALIFIER_TAG, qualifierSIP)
        return true
    }

    protected fun prepareQualifierVariants(
              p: ET
            , variants: MutableMap<String, FormatSet>
            , context: VariantConstructionContext
    ) {
        val qualifierVariants = getQualifierVariants(p, context)
        if (qualifierVariants.isEmpty()) { return }
        variants.put(QUALIFIER_TAG, qualifierVariants)
    }

    protected fun getQualifierVariants(
            p: ET, context: VariantConstructionContext
    ): FormatSet {
        val qualifierExp = getQualifier(p)
        if (qualifierExp == null) { return printer.getEmptySet() }
        val variants = printer.getVariants(qualifierExp, context)
        return variants
    }


    private fun getQualifier(p: ET): PsiElement? =
        when(p) {
            is PsiReferenceExpression      -> p.getQualifierExpression()
            is PsiNewExpression            -> p.getQualifier()
            is PsiJavaCodeReferenceElement -> p.getQualifier()
            else -> null
        }
}