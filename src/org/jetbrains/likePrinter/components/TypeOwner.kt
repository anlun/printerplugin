package org.jetbrains.likePrinter.components

import com.intellij.psi.PsiVariable
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.Template
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import org.jetbrains.likePrinter.util.psiElement.toSmartInsertPlace
import org.jetbrains.format.Format
import org.jetbrains.likePrinter.util.psiElement.getFillConstant
import org.jetbrains.likePrinter.util.psiElement.getNotNullTextRange
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiTypeCastExpression
import com.intellij.psi.PsiInstanceOfExpression

import com.intellij.psi.PsiClassObjectAccessExpression
import org.jetbrains.likePrinter.util.box.Box
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public interface TypeOwner<ET: PsiElement, T: Template<SmartInsertPlace>>: PsiElementComponent<ET, SmartInsertPlace, T> {
    final val TYPE_TAG: String
        get() = "type"

    protected fun addTypeToInsertPlaceMap(p: ET, insertPlaceMap: MutableMap<String, SmartInsertPlace>): Boolean =
            addTypeToInsertPlaceMap(p, insertPlaceMap, 0)

    protected fun addTypeToInsertPlaceMap(
              p: ET
            , insertPlaceMap: MutableMap<String, SmartInsertPlace>
            , delta: Int
    ): Boolean {
        val typeElement = getType(p)
        if (typeElement == null) { return false }
        insertPlaceMap.put(TYPE_TAG,
                SmartInsertPlace(
                          typeElement.getNotNullTextRange().shiftRight(delta)
                        , typeElement.getFillConstant()
                        , Box.getEverywhereSuitable()
                )
        )
        return true
    }

    protected fun prepareTypeVariants(
              p: ET
            , variants: MutableMap<String, FormatSet>
            , context: VariantConstructionContext
    ) {
        val typeVariants = getTypeVariants(p, context)
        if (typeVariants.isEmpty()) { return }
        variants.put(TYPE_TAG, typeVariants)
    }

    protected fun getTypeVariants(p: ET, context: VariantConstructionContext): FormatSet {
        val typeElement = getType(p)
        if (typeElement == null) { return printer.getEmptySet() }
        val variants = printer.getVariants(typeElement, context)
        return variants
    }

    private fun getType(p: PsiElement): PsiElement? =
            when(p) {
                is          PsiVariable           -> p.getTypeElement()
                is          PsiTypeCastExpression -> p.getCastType()
                is        PsiInstanceOfExpression -> p.getCheckType()
                is PsiClassObjectAccessExpression -> p.getOperand()
                else -> null
            }
}