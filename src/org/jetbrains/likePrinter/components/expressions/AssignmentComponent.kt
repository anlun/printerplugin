package org.jetbrains.likePrinter.components.expressions

import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import com.intellij.psi.PsiAssignmentExpression
import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import java.util.HashMap
import org.jetbrains.likePrinter.components.EmptyUpdateComponent

import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public class AssignmentComponent(
        printer: Printer
): PsiElementComponent <PsiAssignmentExpression, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>(printer)
 , ExpressionComponent <PsiAssignmentExpression, PsiTemplate<SmartInsertPlace>>
 , OperationSignOwner  <PsiAssignmentExpression, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>
 , LOperandOwner       <PsiAssignmentExpression, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>
 , ROperandOwner       <PsiAssignmentExpression, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>
 , EmptyUpdateComponent<PsiAssignmentExpression, SmartInsertPlace, PsiTemplate<SmartInsertPlace>> {
    
    override public fun getTemplateFromElement(newP: PsiAssignmentExpression): PsiTemplate<SmartInsertPlace>? {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()

        addOperationSignToInsertPlaceMap(newP, insertPlaceMap)
        addLOperandToInsertPlaceMap     (newP, insertPlaceMap)
        addROperandToInsertPlaceMap     (newP, insertPlaceMap)

        val contentRelation = getContentRelation(newP.getText() ?: "", insertPlaceMap)
        return PsiTemplate(newP, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    override protected fun prepareSubtreeVariants(
            p: PsiAssignmentExpression, context: VariantConstructionContext
    ): Map<String, FormatSet> {
        val variants = HashMap<String, FormatSet>()

        prepareOperationSignVariants(p, variants, context)
        prepareLOperandVariants     (p, variants, context)
        prepareROperandVariants     (p, variants, context)

        return variants
    }

    override protected fun getTags(p: PsiAssignmentExpression) = setOf(L_OPERAND_TAG, R_OPERAND_TAG, OPERATION_SIGN_TAG)
    override protected fun isTemplateSuitable(p: PsiAssignmentExpression, tmplt: PsiTemplate<SmartInsertPlace>) = true
}