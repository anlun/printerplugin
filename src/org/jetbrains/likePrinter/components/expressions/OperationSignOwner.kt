package org.jetbrains.likePrinter.components.expressions

import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import com.intellij.psi.PsiExpression
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.Template
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiPostfixExpression
import org.jetbrains.likePrinter.util.psiElement.toSmartInsertPlace
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import com.intellij.psi.PsiPrefixExpression
import com.intellij.psi.PsiAssignmentExpression
import com.intellij.psi.PsiBinaryExpression

import com.intellij.psi.PsiPolyadicExpression
import org.jetbrains.likePrinter.util.box.Box
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public interface OperationSignOwner<ET: PsiExpression, IPT: SmartInsertPlace, T: Template<IPT>> : PsiElementComponent<ET, IPT, T> {
    final val OPERATION_SIGN_TAG: String
        get() = "operation sign"

    protected fun addOperationSignToInsertPlaceMap(p: ET, insertPlaceMap: MutableMap<String, SmartInsertPlace>): Boolean =
            addOperationSignToInsertPlaceMap(p, insertPlaceMap, 0)

    protected fun addOperationSignToInsertPlaceMap(
              p: ET
            , insertPlaceMap: MutableMap<String, SmartInsertPlace>
            , delta: Int
    ): Boolean {
        val opSIP = getOperationSign(p)?.toSmartInsertPlace()
        if (opSIP == null) { return false }
        insertPlaceMap.put(OPERATION_SIGN_TAG,
                SmartInsertPlace(
                          opSIP.range.shiftRight(delta)
                        , opSIP.fillConstant
                        , Box.getEverywhereSuitable()
                )
        )
        return true
    }

    protected fun prepareOperationSignVariants(
              p: ET
            , variants: MutableMap<String, FormatSet>
            , context: VariantConstructionContext
    ) {
        val opVariants = getOperationSignVariants(p, context)
        if (opVariants.isEmpty()) { return }
        variants.put(OPERATION_SIGN_TAG, opVariants)
    }

    protected fun getOperationSignVariants(p: ET, context: VariantConstructionContext): FormatSet {
        val opSign = getOperationSign(p)
        if (opSign == null) { return printer.getEmptySet() }
        val variants = printer.getVariants(opSign, context)
        return variants
    }

    private fun getOperationSign(p: PsiElement): PsiElement? =
            when(p) {
                is PsiAssignmentExpression -> p.getOperationSign()
                is     PsiBinaryExpression -> p.getOperationSign()

                is     PsiPrefixExpression -> p.getOperationSign()
                is    PsiPostfixExpression -> p.getOperationSign()
                else -> null
            }
}