package org.jetbrains.likePrinter.components.expressions

import com.intellij.psi.PsiExpression
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.Template
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiBinaryExpression
import com.intellij.psi.PsiAssignmentExpression
import org.jetbrains.likePrinter.util.psiElement.toSmartInsertPlace
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext

import com.intellij.psi.PsiPolyadicExpression
import org.jetbrains.likePrinter.util.box.Box
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public interface ROperandOwner<ET: PsiExpression, IPT: SmartInsertPlace, T: Template<IPT>>: PsiElementComponent<ET, IPT, T> {
    final val R_OPERAND_TAG: String
        get() = "right operand"

    protected fun addROperandToInsertPlaceMap(p: ET, insertPlaceMap: MutableMap<String, SmartInsertPlace>): Boolean =
            addROperandToInsertPlaceMap(p, insertPlaceMap, 0)

    protected fun addROperandToInsertPlaceMap(
              p: ET
            , insertPlaceMap: MutableMap<String, SmartInsertPlace>
            , delta: Int
    ): Boolean {
        val operandSIP = getROperand(p)?.toSmartInsertPlace()
        if (operandSIP == null) { return false }
        insertPlaceMap.put(R_OPERAND_TAG,
                SmartInsertPlace(
                          operandSIP.range.shiftRight(delta)
                        , operandSIP.fillConstant
                        , Box.getEverywhereSuitable()
                )
        )
        return true
    }

    protected fun prepareROperandVariants(
              p: ET
            , variants: MutableMap<String, FormatSet>
            , context: VariantConstructionContext
    ) {
        val operandVariants = getROperandVariants(p, context)
        if (operandVariants.isEmpty()) { return }
        variants.put(R_OPERAND_TAG, operandVariants)
    }

    protected fun getROperandVariants(p: ET, context: VariantConstructionContext): FormatSet {
        val operand = getROperand(p)
        if (operand == null) { return printer.getEmptySet() }
        val variants = printer.getVariants(operand, context)
        return variants
    }

    private fun getROperand(p: PsiElement): PsiElement? =
            when(p) {
                is     PsiBinaryExpression -> p.getROperand()
                is   PsiPolyadicExpression -> p.getOperands()[1]
                is PsiAssignmentExpression -> p.getRExpression()
                else -> null
            }
}