package org.jetbrains.likePrinter.components.expressions

import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import com.intellij.psi.PsiInstanceOfExpression
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import org.jetbrains.likePrinter.components.EmptyUpdateComponent
import org.jetbrains.likePrinter.components.TypeOwner
import java.util.HashMap
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext

import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public class InstanceOfComponent(
        printer: Printer
): PsiElementComponent <PsiInstanceOfExpression, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>(printer)
 , ExpressionComponent <PsiInstanceOfExpression, PsiTemplate<SmartInsertPlace>>
 , EmptyUpdateComponent<PsiInstanceOfExpression, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>
 , OperandOwner        <PsiInstanceOfExpression, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>
 , TypeOwner           <PsiInstanceOfExpression, PsiTemplate<SmartInsertPlace>> {

    override public fun getTemplateFromElement(newP: PsiInstanceOfExpression): PsiTemplate<SmartInsertPlace>? {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()

        addOperandToInsertPlaceMap(newP, insertPlaceMap)
        addTypeToInsertPlaceMap   (newP, insertPlaceMap)

        val contentRelation = getContentRelation(newP.getText() ?: "", insertPlaceMap)
        return PsiTemplate(newP, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    override protected fun prepareSubtreeVariants(
            p: PsiInstanceOfExpression, context: VariantConstructionContext
    ): Map<String, FormatSet> {
        val variants = HashMap<String, FormatSet>()

        prepareOperandVariants(p, variants, context)
        prepareTypeVariants   (p, variants, context)

        return variants
    }

    override protected fun getTags(p: PsiInstanceOfExpression) = setOf(OPERAND_TAG, TYPE_TAG)
    override protected fun isTemplateSuitable(p: PsiInstanceOfExpression, tmplt: PsiTemplate<SmartInsertPlace>) = true
}