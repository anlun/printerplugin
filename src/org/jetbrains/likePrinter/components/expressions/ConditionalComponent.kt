package org.jetbrains.likePrinter.components.expressions

import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import org.jetbrains.likePrinter.printer.Printer
import com.intellij.psi.PsiConditionalExpression
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import org.jetbrains.likePrinter.components.ConditionOwner
import java.util.HashMap
import org.jetbrains.likePrinter.util.psiElement.toSmartInsertPlace
import org.jetbrains.likePrinter.components.EmptyUpdateComponent

import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public class ConditionalComponent(
        printer: Printer
): PsiElementComponent <PsiConditionalExpression, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>(printer)
 , ExpressionComponent <PsiConditionalExpression, PsiTemplate<SmartInsertPlace>>
 , ConditionOwner      <PsiConditionalExpression, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>
 , EmptyUpdateComponent<PsiConditionalExpression, SmartInsertPlace, PsiTemplate<SmartInsertPlace>> {
    final val THEN_BRANCH_TAG: String
        get() = "then"
    final val ELSE_BRANCH_TAG: String
        get() = "else"

    override public fun getTemplateFromElement(newP: PsiConditionalExpression): PsiTemplate<SmartInsertPlace>? {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()

        addConditionToInsertPlaceMap(newP, insertPlaceMap)

        val thenExpression = newP.getThenExpression()
        if (thenExpression == null) { return null }
        insertPlaceMap.put(THEN_BRANCH_TAG, thenExpression.toSmartInsertPlace())

        val elseExpression = newP.getElseExpression()
        if (elseExpression == null) { return null }
        insertPlaceMap.put(ELSE_BRANCH_TAG, elseExpression.toSmartInsertPlace())

        val contentRelation = getContentRelation(newP.getText() ?: "", insertPlaceMap)
        return PsiTemplate(newP, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    override protected fun prepareSubtreeVariants(
            p: PsiConditionalExpression, context: VariantConstructionContext
    ): Map<String, FormatSet> {
        val variants = HashMap<String, FormatSet>()

        prepareConditionPart(p, variants, context)

        val thenVariants = getThenExpressionVariants(p, context)
        if (thenVariants.isEmpty()) { return variants }
        variants.put(THEN_BRANCH_TAG, thenVariants)

        val elseVariants = getElseExpressionVariants(p, context)
        if (elseVariants.isEmpty()) { return variants }
        variants.put(ELSE_BRANCH_TAG, elseVariants)

        return variants
    }

    protected fun getThenExpressionVariants(
            p: PsiConditionalExpression, context: VariantConstructionContext
    ): FormatSet {
        val thenExpression = p.getThenExpression()
        if (thenExpression == null) { return printer.getEmptySet() }
        return printer.getVariants(thenExpression, context)
    }

    protected fun getElseExpressionVariants(
            p: PsiConditionalExpression, context: VariantConstructionContext
    ): FormatSet {
        val elseExpression = p.getElseExpression()
        if (elseExpression == null) { return printer.getEmptySet() }
        return printer.getVariants(elseExpression, context)
    }

    override protected fun getTags(p: PsiConditionalExpression) = setOf(CONDITION_TAG, THEN_BRANCH_TAG, ELSE_BRANCH_TAG)
    override protected fun isTemplateSuitable(p: PsiConditionalExpression, tmplt: PsiTemplate<SmartInsertPlace>) = true
}
