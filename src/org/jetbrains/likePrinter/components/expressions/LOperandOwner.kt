package org.jetbrains.likePrinter.components.expressions

import com.intellij.psi.PsiExpression
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.Template
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiBinaryExpression
import com.intellij.psi.PsiAssignmentExpression
import org.jetbrains.likePrinter.util.psiElement.toSmartInsertPlace

import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import com.intellij.psi.PsiPolyadicExpression
import org.jetbrains.likePrinter.util.box.Box
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public interface LOperandOwner<ET: PsiExpression, IPT: SmartInsertPlace, T: Template<IPT>>: PsiElementComponent<ET, IPT, T> {
    final val L_OPERAND_TAG: String
        get() = "left operand"

    protected fun addLOperandToInsertPlaceMap(p: ET, insertPlaceMap: MutableMap<String, SmartInsertPlace>): Boolean =
            addLOperandToInsertPlaceMap(p, insertPlaceMap, 0)

    protected fun addLOperandToInsertPlaceMap(
              p: ET
            , insertPlaceMap: MutableMap<String, SmartInsertPlace>
            , delta: Int
    ): Boolean {
        val operandSIP = getLOperand(p)?.toSmartInsertPlace()
        if (operandSIP == null) { return false }
        insertPlaceMap.put(L_OPERAND_TAG,
                SmartInsertPlace(
                          operandSIP.range.shiftRight(delta)
                        , operandSIP.fillConstant
                        , Box.getEverywhereSuitable()
                )
        )
        return true
    }

    protected fun prepareLOperandVariants(
              p: ET
            , variants: MutableMap<String, FormatSet>
            , context: VariantConstructionContext
    ) {
        val operandVariants = getLOperandVariants(p, context)
        if (operandVariants.isEmpty()) { return }
        variants.put(L_OPERAND_TAG, operandVariants)
    }

    protected fun getLOperandVariants(p: ET, context: VariantConstructionContext): FormatSet {
        val operand = getLOperand(p)
        if (operand == null) { return printer.getEmptySet() }
        val variants = printer.getVariants(operand, context)
        return variants
    }

    private fun getLOperand(p: PsiElement): PsiElement? =
            when(p) {
                is     PsiBinaryExpression -> p.getLOperand()
                is   PsiPolyadicExpression -> p.getOperands()[0]
                is PsiAssignmentExpression -> p.getLExpression()
                else -> null
            }
}