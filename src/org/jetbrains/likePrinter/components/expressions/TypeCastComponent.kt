package org.jetbrains.likePrinter.components.expressions

import org.jetbrains.likePrinter.printer.Printer
import com.intellij.psi.PsiTypeCastExpression
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import java.util.HashMap
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import org.jetbrains.likePrinter.components.TypeOwner
import org.jetbrains.likePrinter.components.EmptyUpdateComponent

import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public class TypeCastComponent(
        printer: Printer
): PsiElementComponent <PsiTypeCastExpression, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>(printer)
 , ExpressionComponent <PsiTypeCastExpression, PsiTemplate<SmartInsertPlace>>
 , OperandOwner        <PsiTypeCastExpression, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>
 , TypeOwner           <PsiTypeCastExpression, PsiTemplate<SmartInsertPlace>>
 , EmptyUpdateComponent<PsiTypeCastExpression, SmartInsertPlace, PsiTemplate<SmartInsertPlace>> {
    
    override public fun getTemplateFromElement(newP: PsiTypeCastExpression): PsiTemplate<SmartInsertPlace>? {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()

        addOperandToInsertPlaceMap(newP, insertPlaceMap)
        addTypeToInsertPlaceMap   (newP, insertPlaceMap)

        val contentRelation = getContentRelation(newP.getText() ?: "", insertPlaceMap)
        return PsiTemplate(newP, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    override protected fun prepareSubtreeVariants(
            p: PsiTypeCastExpression, context: VariantConstructionContext
    ): Map<String, FormatSet> {
        val variants = HashMap<String, FormatSet>()

        prepareOperandVariants(p, variants, context)
        prepareTypeVariants   (p, variants, context)

        return variants
    }

    override protected fun getTags(p: PsiTypeCastExpression) = setOf(OPERAND_TAG, TYPE_TAG)
    override protected fun isTemplateSuitable(p: PsiTypeCastExpression, tmplt: PsiTemplate<SmartInsertPlace>) = true
}