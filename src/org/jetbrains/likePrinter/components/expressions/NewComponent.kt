package org.jetbrains.likePrinter.components.expressions

import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import com.intellij.psi.PsiNewExpression
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import org.jetbrains.likePrinter.components.EmptyUpdateComponent
import org.jetbrains.likePrinter.components.classes.ArgumentListOwner
import java.util.HashMap
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import org.jetbrains.format.Format
import org.jetbrains.format.emptyFormatList
import org.jetbrains.likePrinter.util.psiElement.toSmartInsertPlace
import java.util.HashSet
import org.jetbrains.likePrinter.util.psiElement.hasElement
import com.intellij.openapi.util.TextRange
import com.intellij.psi.impl.PsiImplUtil
import com.intellij.psi.JavaTokenType
import org.jetbrains.likePrinter.util.string.getFillConstant

import org.jetbrains.likePrinter.components.QualifierOwner
import org.jetbrains.likePrinter.components.ArrayDimensionOwner
import java.util.ArrayList
import org.jetbrains.format.div
import org.jetbrains.format.FormatSet
import org.jetbrains.likePrinter.util.base.fillListByWidth

/**
 * User: anlun
 */
public class NewComponent(
        printer: Printer
): PsiElementComponent <PsiNewExpression, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>(printer)
 , ExpressionComponent <PsiNewExpression, PsiTemplate<SmartInsertPlace>>
 , EmptyUpdateComponent<PsiNewExpression, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>
 , QualifierOwner      <PsiNewExpression, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>
 , ArrayDimensionOwner <PsiNewExpression, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>
 , ArgumentListOwner   <PsiNewExpression, SmartInsertPlace, PsiTemplate<SmartInsertPlace>> {
    final val CLASS_REFERENCE_TAG: String
        get() = "class reference"

    final val ANONYMOUS_CLASS_TAG: String
        get() = "anonymous class"

    override public fun getTemplateFromElement(newP: PsiNewExpression): PsiTemplate<SmartInsertPlace>? {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()

        val anonymousClass = newP.getAnonymousClass()

        if (anonymousClass != null) {
            insertPlaceMap.put(ANONYMOUS_CLASS_TAG, anonymousClass.toSmartInsertPlace())
        } else {
            val classReference = newP.getClassOrAnonymousClassReference() ?: return null
            insertPlaceMap.put(CLASS_REFERENCE_TAG, classReference.toSmartInsertPlace())

            addArrayDimensionToInsertPlaceMap(newP, insertPlaceMap, 0)
              addArgumentListToInsertPlaceMap(newP, insertPlaceMap, 0)
                 addQualifierToInsertPlaceMap(newP, insertPlaceMap, 0)
        }

        val contentRelation = getContentRelation(newP.getText() ?: "", insertPlaceMap)
        return PsiTemplate(newP, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    override protected fun prepareSubtreeVariants(
            p: PsiNewExpression, context: VariantConstructionContext
    ): Map<String, FormatSet> {
        val anonymousClass = p.getAnonymousClass()
        if (anonymousClass != null) {
            return mapOf(Pair(ANONYMOUS_CLASS_TAG, getAnonymousClassVariants(p, context)))
        }

        val variants = HashMap<String, FormatSet>()

        prepareArgumentListVariants(p, variants, context)
           prepareQualifierVariants(p, variants, context)

        val classReferenceVariants = getClassReferenceVariants(p, context)
        variants.put(CLASS_REFERENCE_TAG, classReferenceVariants)

        prepareArrayDimensionVariants(p, variants, context)

        return variants
    }

    protected fun getAnonymousClassVariants(p: PsiNewExpression, context: VariantConstructionContext): FormatSet {
        val anonymousClass = p.getAnonymousClass()
        if (anonymousClass == null) { return printer.getEmptySet() }
        val variants = printer.getVariants(anonymousClass, context)
        return variants
    }

    protected fun getClassReferenceVariants(p: PsiNewExpression, context: VariantConstructionContext): FormatSet {
        val classReference = p.getClassOrAnonymousClassReference()
        if (classReference == null) { return printer.getEmptySet() }
        val variants = printer.getVariants(classReference, context)
        return variants
    }

    override protected fun getTags(p: PsiNewExpression): Set<String> {
        val anonymousClass = p.getAnonymousClass()
        if (anonymousClass != null) {
            return setOf(ANONYMOUS_CLASS_TAG)
        }

        val set = HashSet<String>()

        if (hasElement(p.getArgumentList())) { set.add(ARGUMENT_LIST_TAG)   }
        set.add(CLASS_REFERENCE_TAG)
        if (hasElement(p.getQualifier())   ) { set.add(QUALIFIER_TAG)       }
        if (p.hasArrayDimensions()         ) { set.add(ARRAY_DIMENSION_TAG) }

        return set
    }

    private fun PsiNewExpression.hasArrayDimensions(): Boolean {
        val arrayDimensionSize = getType()?.getArrayDimensions()
        return !(arrayDimensionSize == null || arrayDimensionSize <= 0)
    }

    override protected fun getArrayDimensionVariants(p: PsiNewExpression, context: VariantConstructionContext): FormatSet {
        //TODO: may be use some sort of templates

        val arrayDimensionSize = p.getType()?.getArrayDimensions()
        if (arrayDimensionSize == null || arrayDimensionSize <= 0) { return printer.getEmptySet() }

        val arrayDimensionExpressionVariants = p.getArrayDimensions() map { e ->
            Format.line("[") / printer.getVariants(e, context) / Format.line("]")
        }

        val arrayDimensionVariants = ArrayList<FormatSet>(arrayDimensionSize)
        arrayDimensionVariants.addAll(arrayDimensionExpressionVariants)

        val arrayExpressionDimensionCount = arrayDimensionExpressionVariants.size()
        for (i in 1..(arrayDimensionSize - arrayExpressionDimensionCount)) {
            arrayDimensionVariants.add(printer.getInitialSet(Format.line("[]")))
        }
        return arrayDimensionVariants.fillListByWidth(
                printer, context.widthToSuit, {x -> x}, Format.empty
        )
    }

    override protected fun isTemplateSuitable(p: PsiNewExpression, tmplt: PsiTemplate<SmartInsertPlace>) = true
}