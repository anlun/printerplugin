package org.jetbrains.likePrinter.components.expressions

import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import com.intellij.psi.PsiBinaryExpression
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import org.jetbrains.likePrinter.components.EmptyUpdateComponent
import java.util.HashMap
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext

import com.intellij.psi.PsiPolyadicExpression
import org.jetbrains.likePrinter.templateBase.template.Template
import com.intellij.openapi.util.TextRange
import com.intellij.psi.PsiExpression
import java.util.ArrayList
import org.jetbrains.likePrinter.util.psiElement.getVariants
import org.jetbrains.format.Format
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public class PolyadicComponent(
        printer: Printer
): PsiElementComponent <PsiPolyadicExpression, SmartInsertPlace, Template<SmartInsertPlace>>(printer)
 , ExpressionComponent <PsiPolyadicExpression, Template<SmartInsertPlace>>
 , EmptyUpdateComponent<PsiPolyadicExpression, SmartInsertPlace, Template<SmartInsertPlace>> {

    //TODO: maybe use BinaryExpression templates
    override public fun getTemplateFromElement(
            newP: PsiPolyadicExpression
    ): PsiTemplate<SmartInsertPlace>? = null

    override protected fun prepareSubtreeVariants(
            p: PsiPolyadicExpression, context: VariantConstructionContext
    ): Map<String, FormatSet> {
        val variants = HashMap<String, FormatSet>()
        variants.put(FULL_CONSTRUCTION_TAG, getVariantsWithoutTmplt_1(p, context))
        return variants
    }

    override protected fun getTags(p: PsiPolyadicExpression) = setOf(FULL_CONSTRUCTION_TAG)
    override protected fun isTemplateSuitable(p: PsiPolyadicExpression, tmplt: Template<SmartInsertPlace>) = true

    override protected fun getTemplates() = listOf(template)

    private final val template = Template<SmartInsertPlace>(
              JUNK_TEXT
            , getFullConstructionInsertPlaceMap         ()
            , getFullConstructionTagPlaceToLineNumberMap()
            , getFullConstructionLineEquationMap        ()
    )

    private fun getVariantsWithoutTmplt_1(p: PsiPolyadicExpression, context: VariantConstructionContext): FormatSet {
        val elementsWithOperations = p.getElementsWithOperations()
        val operations = elementsWithOperations.map { p -> p.first }
        val elements   = elementsWithOperations.map { p -> p.second }

        return getElementsVariants(elements, context, { n -> Format.line(" " + operations.get(n) + " ") })
    }

    private fun PsiPolyadicExpression.getElementsWithOperations(): ArrayList< Pair<String, PsiExpression> > {
        val operation = getOperationSign()
        val operationPriority = operation.getPriority()
        val result = ArrayList< Pair<String, PsiExpression> >()
        var operandNumber = -1

        fun curOperationSymbol(): String {
            if (operandNumber == 0) { return "" }
            return operation
        }

        for (operand in getOperands()) {
            operandNumber++

            if (operand !is PsiPolyadicExpression || operand.getOperationPriority() > operationPriority) {
                result.add(Pair(curOperationSymbol(), operand))
                continue
            }

            val operandSubList = operand.getElementsWithOperations()
            val firstSubListElement = operandSubList.firstOrNull()
            if (firstSubListElement == null) {
                operandNumber = Math.max(0, operandNumber - 1)
                continue
            }
            operandSubList.set(0, Pair(curOperationSymbol(), firstSubListElement.second))
            result.addAll(operandSubList)
        }

        return result
    }

    /// According to http://docs.oracle.com/javase/tutorial/java/nutsandbolts/operators.html
    private fun String.getPriority(): Int =
        when (this) {
            "*", "/", "%"     -> 3
            "+", "-"          -> 4
            "<<", ">>", ">>>" -> 5
            "<", "<=", ">", ">=", "instanceof" -> 6
            "==", "!=" -> 7
            "&"        -> 8
            "^"        -> 9
            "|"        -> 10
            "&&"       -> 11
            "||"       -> 12

            else -> -1
        }

    private fun PsiPolyadicExpression.getOperationSign(): String {
        val operands = getOperands()
        if (operands.size() < 2) { return "" }

        val secondOperand = operands[1]
        val operation = getTokenBeforeOperand(secondOperand)
        return operation?.getText() ?: ""
    }

    private fun PsiPolyadicExpression.getOperationPriority(): Int = getOperationSign().getPriority()
}