package org.jetbrains.likePrinter.components.expressions

import org.jetbrains.likePrinter.printer.Printer
import com.intellij.psi.PsiClassObjectAccessExpression
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import org.jetbrains.likePrinter.components.EmptyUpdateComponent
import org.jetbrains.likePrinter.components.TypeOwner
import java.util.HashMap

import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public class ClassObjectAccessComponent(
        printer: Printer
): PsiElementComponent <PsiClassObjectAccessExpression, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>(printer)
 , ExpressionComponent <PsiClassObjectAccessExpression, PsiTemplate<SmartInsertPlace>>
 , TypeOwner           <PsiClassObjectAccessExpression, PsiTemplate<SmartInsertPlace>>
 , EmptyUpdateComponent<PsiClassObjectAccessExpression, SmartInsertPlace, PsiTemplate<SmartInsertPlace>> {

    override public fun getTemplateFromElement(newP: PsiClassObjectAccessExpression): PsiTemplate<SmartInsertPlace>? {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()

        addTypeToInsertPlaceMap(newP, insertPlaceMap)

        val contentRelation = getContentRelation(newP.getText() ?: "", insertPlaceMap)
        return PsiTemplate(newP, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    override protected fun prepareSubtreeVariants(
            p: PsiClassObjectAccessExpression, context: VariantConstructionContext
    ): Map<String, FormatSet> {
        val variants = HashMap<String, FormatSet>()

        prepareTypeVariants(p, variants, context)

        return variants
    }

    override protected fun getTags(p: PsiClassObjectAccessExpression) = setOf(TYPE_TAG)
    override protected fun isTemplateSuitable(
            p: PsiClassObjectAccessExpression, tmplt: PsiTemplate<SmartInsertPlace>
    ): Boolean = true
}