package org.jetbrains.likePrinter.components

import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import com.intellij.psi.PsiMethod
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import com.intellij.psi.PsiElementFactory
import com.intellij.openapi.project.Project
import java.util.HashMap
import org.jetbrains.likePrinter.util.psiElement.hasElement
import org.jetbrains.likePrinter.util.psiElement.toSmartInsertPlace
import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.util.base.IncorrectPsiElementException
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import org.jetbrains.likePrinter.util.psiElement.getFillConstant
import org.jetbrains.likePrinter.util.psiElement.getNotNullTextRange
import org.jetbrains.format.emptyFormatList
import java.util.HashSet
import com.intellij.psi.PsiCodeBlock
import com.intellij.psi.PsiBlockStatement
import com.intellij.psi.PsiAnnotationMethod
import org.jetbrains.likePrinter.util.psiElement.getCorrectTextOffset
import org.jetbrains.format.FormatSet


/**
 * User: anlun
 */
public class AnnotationMethodComponent(
        printer: Printer
): PsiElementComponent   <PsiAnnotationMethod, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>(printer)
 , ModifierListOwner     <PsiAnnotationMethod, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>
 , NameOwner             <PsiAnnotationMethod, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>
 , TypeParameterListOwner<PsiAnnotationMethod, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>
 , ParameterListOwner    <PsiAnnotationMethod, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>
 , ReturnTypeOwner       <PsiAnnotationMethod, PsiTemplate<SmartInsertPlace>>
 , ThrowsListOwner       <PsiAnnotationMethod, PsiTemplate<SmartInsertPlace>>
 , EmptyUpdateComponent  <PsiAnnotationMethod, SmartInsertPlace, PsiTemplate<SmartInsertPlace>> {

    final val DEFAULT_VALUE_TAG: String
        get() = "default value"

    override protected fun getNewElement(text: String, elementFactory: PsiElementFactory): PsiAnnotationMethod? {
        val methodToAnnotationTypeText = { methodText: String ->
            "@interface A {\n$methodText\n};"
        }
        val dummyClass = elementFactory.createClassFromText(methodToAnnotationTypeText(text), null)
        val innerClasses = dummyClass.getAllInnerClasses()
        if (innerClasses.isEmpty()) { return null }
        val aAnnotationType = innerClasses[0]
        val methods = aAnnotationType.getMethods()
        if (methods.isEmpty()) { return null }
        return methods[0] as? PsiAnnotationMethod
    }

    override public fun getTemplateFromElement(newP: PsiAnnotationMethod): PsiTemplate<SmartInsertPlace>? {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()

        val negShift = -newP.getCorrectTextOffset()

        if (!addNameToInsertPlaceMap(newP, insertPlaceMap, negShift)) { return null }
             addModifierListToInsertPlaceMap(newP, insertPlaceMap, negShift)
            addParameterListToInsertPlaceMap(newP, insertPlaceMap, negShift)
        addTypeParameterListToInsertPlaceMap(newP, insertPlaceMap, negShift)

        addReturnTypeToInsertPlaceMap(newP, insertPlaceMap, negShift)
        addThrowsListToInsertPlaceMap(newP, insertPlaceMap, negShift)

        val defaultValue = newP.getDefaultValue()
        if (defaultValue != null) {
            insertPlaceMap.put(DEFAULT_VALUE_TAG, defaultValue.toSmartInsertPlace().shiftRight(negShift))
        }

        val contentRelation = getContentRelation(newP.getText() ?: "", insertPlaceMap)

        return PsiTemplate(newP, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    protected fun getDefaultValueVariants(p: PsiAnnotationMethod, context: VariantConstructionContext): FormatSet {
        val defaultValue = p.getDefaultValue()
        if (defaultValue == null) { return printer.getEmptySet() }
        return printer.getVariants(defaultValue, context)
    }

    override protected fun prepareSubtreeVariants(
              p: PsiAnnotationMethod
            , context: VariantConstructionContext
    ): Map<String, FormatSet> {
        val variants = HashMap<String, FormatSet>()

        prepareModifierListVariants(p, variants, context)

        val returnTypeVariants = getReturnTypeVariants(p, context)
        if (!returnTypeVariants.isEmpty()) {
            variants.put(RETURN_TYPE_TAG, returnTypeVariants)
        }

        prepareNameVariants         (p, variants, context)
        prepareParameterListVariants(p, variants, context)

        val throwsListVariants = getThrowsListVariants(p, context)
        if (!throwsListVariants.isEmpty()) {
            variants.put(THROWS_LIST_TAG, throwsListVariants)
        }

        prepareTypeParameterListVariants(p, variants, context)

        val defaultValueVariants = getDefaultValueVariants(p, context)
        if (!defaultValueVariants.isEmpty()) {
            variants.put(DEFAULT_VALUE_TAG, defaultValueVariants)
        }

        return variants
    }

    override protected fun getTags(p: PsiAnnotationMethod): Set<String> {
        val set = HashSet<String>()

        if (hasElement(p.getModifierList())) { set.add(MODIFIER_LIST_TAG) }
        if (p.getReturnType() != null)       { set.add(  RETURN_TYPE_TAG) }
        set.add(NAME_TAG)
        if (hasElement(p.    getParameterList())) { set.add(     PARAMETER_LIST_TAG) }
        if (hasElement(p.       getThrowsList())) { set.add(        THROWS_LIST_TAG) }
        if (hasElement(p.getTypeParameterList())) { set.add(TYPE_PARAMETER_LIST_TAG) }

        if (hasElement(p.getDefaultValue())) { set.add(DEFAULT_VALUE_TAG) }

        return set
    }

    override protected fun isTemplateSuitable(p: PsiAnnotationMethod, tmplt: PsiTemplate<SmartInsertPlace>): Boolean = true
}