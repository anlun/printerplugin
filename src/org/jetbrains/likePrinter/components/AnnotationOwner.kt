package org.jetbrains.likePrinter.components

import com.intellij.psi.PsiAnnotationOwner
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.Template
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiAnnotation
import org.jetbrains.likePrinter.util.psiElement.getTextRange
import org.jetbrains.likePrinter.util.string.getFillConstant
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import org.jetbrains.format.*
import org.jetbrains.likePrinter.util.box.Box

/**
 * User: anlun
 */
public interface AnnotationOwner<ET: PsiElement, IPT: SmartInsertPlace, T: Template<IPT>>: PsiElementComponent<ET, IPT, T> {
    final val ANNOTATION_TAG: String
        get() = "annotation"

    protected fun addAnnotationsToInsertPlaceMap(p: ET, insertPlaceMap: MutableMap<String, SmartInsertPlace>): Boolean =
            addAnnotationsToInsertPlaceMap(p, insertPlaceMap, 0)

    protected fun addAnnotationsToInsertPlaceMap(
              p: ET
            , insertPlaceMap: MutableMap<String, SmartInsertPlace>
            , delta: Int
    ): Boolean {
        val annotations = getAnnotations(p)
        val annotationTextRange = annotations?.getTextRange()
        if (annotationTextRange == null) { return false }

        val text = p.getContainingFile()?.getText()
        if (text == null) { return false }
        val fillConstant = text.getFillConstant(annotationTextRange)

        insertPlaceMap.put(ANNOTATION_TAG, SmartInsertPlace(annotationTextRange.shiftRight(delta), fillConstant, Box.getEverywhereSuitable()))
        return true
    }

    protected fun prepareAnnotationVariants(
            p: ET, variants: MutableMap<String, FormatSet>, context: VariantConstructionContext
    ) {
        val annotationVariants = getAnnotationVariants(p, context)
        if (annotationVariants.isEmpty()) { return }
        variants.put(ANNOTATION_TAG, annotationVariants)
    }

    protected fun getAnnotationVariants(p: ET, context: VariantConstructionContext): FormatSet {
        val annotations = getAnnotations(p)
        if (annotations == null || annotations.isEmpty()) { return printer.getEmptySet() }

        val annotationVariants = annotations.map { e -> printer.getVariants(e, context) }
        val variants = annotationVariants.fold(printer.getInitialSet(), { r, e -> r - e })
        return variants
    }

    private fun getAnnotations(p: ET): Array<PsiAnnotation>? =
        when(p) {
            is PsiAnnotationOwner -> p.getAnnotations()
            else -> null
        }
}