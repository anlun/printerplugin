package org.jetbrains.likePrinter.components.lists

import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import com.intellij.psi.PsiNameValuePair
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiTemplateGen
import org.jetbrains.likePrinter.components.NameOwner
import org.jetbrains.likePrinter.components.EmptyUpdateComponent
import com.intellij.psi.PsiElementFactory
import org.jetbrains.likePrinter.util.psiElement.getCorrectTextOffset
import java.util.HashMap
import org.jetbrains.likePrinter.util.psiElement.toSmartInsertPlace
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext

import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import java.util.HashSet
import org.jetbrains.likePrinter.util.psiElement.hasElement
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public class NameValuePairComponent(
        printer: Printer
): PsiElementComponent <PsiNameValuePair, SmartInsertPlace, PsiTemplateGen<PsiNameValuePair, SmartInsertPlace>>(printer)
 , NameOwner           <PsiNameValuePair, SmartInsertPlace, PsiTemplateGen<PsiNameValuePair, SmartInsertPlace>>
 , EmptyUpdateComponent<PsiNameValuePair, SmartInsertPlace, PsiTemplateGen<PsiNameValuePair, SmartInsertPlace>> {
    final val VALUE_TAG: String
        get() = "value"

    override protected fun getNewElement(text: String, elementFactory: PsiElementFactory): PsiNameValuePair? {
        val methodText = "@A(\n$text\n) void main() {}"
        val method = elementFactory.createMethodFromText(methodText, null)
        val annotations = method.getModifierList().getAnnotations()
        if (annotations.isEmpty()) { return null }
        val attributes = annotations[0].getParameterList().getAttributes()
        if (attributes.isEmpty()) { return null }
        return attributes[0]
    }

    override public fun getTemplateFromElement(newP: PsiNameValuePair): PsiTemplateGen<PsiNameValuePair, SmartInsertPlace>? {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()

        val negShift = -newP.getCorrectTextOffset()
        addNameToInsertPlaceMap(newP, insertPlaceMap, negShift)

        val value = newP.getValue()
        if (value != null) {
            insertPlaceMap.put(VALUE_TAG, value.toSmartInsertPlace().shiftRight(negShift))
        }

        val contentRelation = getContentRelation(newP.getText() ?: "", insertPlaceMap)
        return PsiTemplateGen(newP, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    protected fun getValueVariants(p: PsiNameValuePair, context: VariantConstructionContext): FormatSet {
        val value = p.getValue()
        if (value == null) { return printer.getEmptySet() }
        return printer.getVariants(value, context)
    }

    override protected fun prepareSubtreeVariants(
              p: PsiNameValuePair
            , context: VariantConstructionContext
    ): Map<String, FormatSet> {
        val variants = HashMap<String, FormatSet>()

        prepareNameVariants         (p, variants, context)
        val valueVariants = getValueVariants(p, context)
        if (!valueVariants.isEmpty()) {
            variants.put(VALUE_TAG, valueVariants)
        }

        return variants
    }

    override protected fun getTags(p: PsiNameValuePair): Set<String> {
        val set = HashSet<String>()

        if (hasElement(p.getNameIdentifier())) { set.add( NAME_TAG) }
        if (hasElement(p.getValue         ())) { set.add(VALUE_TAG) }

        return set
    }

    override protected fun isTemplateSuitable(
            p: PsiNameValuePair, tmplt: PsiTemplateGen<PsiNameValuePair, SmartInsertPlace>
    ): Boolean = true
}