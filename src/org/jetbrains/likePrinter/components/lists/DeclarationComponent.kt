package org.jetbrains.likePrinter.components.lists

import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import org.jetbrains.likePrinter.printer.Printer
import com.intellij.psi.PsiElement
import org.jetbrains.likePrinter.templateBase.template.Template
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext

import java.util.ArrayList
import org.jetbrains.likePrinter.util.psiElement.deleteSpaces
import com.intellij.psi.PsiDeclarationStatement
import com.intellij.psi.PsiMethod
import com.intellij.psi.PsiVariable
import com.intellij.psi.PsiClass
import com.intellij.psi.PsiStatement
import com.intellij.psi.PsiExpression
import org.jetbrains.format.FormatSet


/**
 * User: anlun
 */
public class DeclarationComponent(
        printer: Printer
): PsiElementComponent<PsiDeclarationStatement, SmartInsertPlace, ListTemplate<PsiDeclarationStatement, SmartInsertPlace>>(printer)
 , ListComponent      <PsiDeclarationStatement> {

    override public fun getNormalizedElement(p: PsiDeclarationStatement): PsiDeclarationStatement? {
        val normalizedText = p.deleteSpaces()
//        val declarationToMethodText = { (t: String) ->
//            "void main() {\n$t\n}"
//        }
        val declarationToMethodText = { t: String -> getMethodTextFromListElementText(t) }
        val methodText = declarationToMethodText(normalizedText)
        val method = getMethodByText(methodText)
        if (method == null) { return null }
        val element = getElementFromMethod(method)
        return element
    }

    override public fun getMethodTextFromListElementText(elementText: String): String =
            "void main() {\n$elementText\n}"

    override public fun getElementFromMethod(method: PsiMethod): PsiDeclarationStatement? {
        val body = method.getBody()
        val statements = body?.getStatements()
        if (statements == null || statements.isEmpty()) { return null }
        val statement = statements.get(0) as? PsiDeclarationStatement
        return statement
    }

    override public fun createElement2Tmplt(
                 p: PsiElement
            , list: List<PsiElement>
    ): List<Template<SmartInsertPlace>> {
        return createMethodElemListPairTmpltList(
                p, list
                , { m ->
                    val declarationStatement = getElementFromMethod(m)
                    if (declarationStatement != null) {
                        getList(declarationStatement)
                    } else {
                        ArrayList<PsiElement>()
                    }
                }
        )
    }

    override protected fun isTemplateSuitable(
            p: PsiDeclarationStatement, tmplt: ListTemplate<PsiDeclarationStatement, SmartInsertPlace>
    ): Boolean = true

    override protected fun getList(p: PsiDeclarationStatement): List<PsiElement> {
        //TODO: may be filter all sub class of PsiElement for whom we have printer
        val declaredElements = p.getChildren().filter {
            ch -> ch is PsiClass || ch is PsiVariable || ch is PsiStatement || ch is PsiExpression
        }.sortBy { ch -> ch.getStartOffsetInParent() }

        return declaredElements.toList()
    }

    override protected fun getElementsVariants(
                    p: PsiDeclarationStatement
            ,   tmplt: ListTemplate<PsiDeclarationStatement, SmartInsertPlace>
            , context: VariantConstructionContext
    ): FormatSet = getElementsVariants(p, context, getSeparator_1())
    //TODO: Maybe add fillConstant, or use it from template
}