package org.jetbrains.likePrinter.components.lists

import com.intellij.psi.PsiParameterList
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import org.jetbrains.likePrinter.printer.Printer
import com.intellij.psi.PsiElement
import org.jetbrains.likePrinter.templateBase.template.Template
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext

import com.intellij.psi.PsiMethod
import java.util.ArrayList
import org.jetbrains.likePrinter.util.psiElement.deleteSpaces
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public class ParameterListComponent(
        printer: Printer
): PsiElementComponent<PsiParameterList, SmartInsertPlace, ListTemplate<PsiParameterList, SmartInsertPlace>>(printer)
 , ListComponent      <PsiParameterList> {

    override public fun getNormalizedElement(p: PsiParameterList): PsiParameterList? {
        val normalizedText = p.deleteSpaces()
        val arrayInitializerToMethodText = { t: String ->
            "void main\n$t\n {}"
        }
        val methodText = arrayInitializerToMethodText(normalizedText)
        val method = getMethodByText(methodText)
        if (method == null) { return null }
        val element = getElementFromMethod(method)
        return element
    }

    override public fun getMethodTextFromListElementText(elementText: String): String =
            "void main(\n$elementText\n){}"

    override public fun getElementFromMethod(method: PsiMethod): PsiParameterList? =
        method.getParameterList()

    override public fun createElement2Tmplt(
              p: PsiElement
            , list: List<PsiElement>
    ): List<Template<SmartInsertPlace>> {
        return createMethodElemListPairTmpltList(
                  p, list
                , { m ->
                    val elementFromMethod = getElementFromMethod(m)
                    if (elementFromMethod != null) {
                        getList(elementFromMethod)
                    } else {
                        ArrayList<PsiElement>()
                    }
                }
        )
    }

    override protected fun isTemplateSuitable(
            p: PsiParameterList, tmplt: ListTemplate<PsiParameterList, SmartInsertPlace>
    ): Boolean = true

    override protected fun getList(p: PsiParameterList): List<PsiElement> = p.getParameters().toList()


    override protected fun getElementsVariants(
                    p: PsiParameterList
            ,   tmplt: ListTemplate<PsiParameterList, SmartInsertPlace>
            , context: VariantConstructionContext
    ): FormatSet = getElementsVariants(p, context, getSeparator_1())
}