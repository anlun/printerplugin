package org.jetbrains.likePrinter.components.lists

import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import com.intellij.psi.PsiResourceList
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import com.intellij.psi.PsiElement
import java.util.ArrayList
import com.intellij.psi.PsiTryStatement
import org.jetbrains.likePrinter.templateBase.template.Template
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext

import com.intellij.psi.PsiMethod
import org.jetbrains.format.Format
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public class ResourceListComponent(
        printer: Printer
): PsiElementComponent<PsiResourceList, SmartInsertPlace, ListTemplate<PsiResourceList, SmartInsertPlace>>(printer)
 , ListComponent      <PsiResourceList> {

    override public fun getMethodTextFromListElementText(elementText: String): String =
            "void main(){try(\n$elementText\n){};}"

    override public fun getElementFromMethod(method: PsiMethod): PsiResourceList? {
        val body = method.getBody()
        val statements = body?.getStatements()
        if (statements == null || statements.isEmpty()) { return null }
        val tryStatement = statements[0] as? PsiTryStatement
        return tryStatement?.getResourceList()
    }

    override public fun createElement2Tmplt(
              p: PsiElement
            , list: List<PsiElement>
    ): List<Template<SmartInsertPlace>> {
        return createMethodElemListPairTmpltList(
                p, list
                , { m -> getElementFromMethod(m)?.getResourceVariables() ?: ArrayList() }
        )
    }

    override protected fun getElementsVariants(
                    p: PsiResourceList
            ,   tmplt: ListTemplate<PsiResourceList, SmartInsertPlace>
            , context: VariantConstructionContext
    ): FormatSet = getElementsVariants(p, context, getSeparator_1())
    override public fun getSeparator_1(): Format = Format.line("; ")

    override protected fun isTemplateSuitable(
            p: PsiResourceList, tmplt: ListTemplate<PsiResourceList, SmartInsertPlace>
    ): Boolean = true

    override protected fun getList(p: PsiResourceList): List<PsiElement> = p.getResourceVariables()
}