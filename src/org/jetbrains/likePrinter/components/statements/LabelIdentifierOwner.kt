package org.jetbrains.likePrinter.components.statements

import com.intellij.psi.PsiElement
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.Template
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import com.intellij.psi.PsiBreakStatement
import com.intellij.psi.PsiContinueStatement
import org.jetbrains.likePrinter.components.EmptyUpdateComponent
import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import java.util.HashMap
import org.jetbrains.likePrinter.util.psiElement.toSmartInsertPlace
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext

import org.jetbrains.likePrinter.util.psiElement.hasElement
import java.util.HashSet
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public interface LabelIdentifierOwner<ET: PsiElement>
: PsiElementComponent <ET, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>
, EmptyUpdateComponent<ET, SmartInsertPlace, PsiTemplate<SmartInsertPlace>> {
    final val LABEL_IDENTIFIER_TAG: String
        get() = "label identifier"

    override public fun getTemplateFromElement(newP: ET): PsiTemplate<SmartInsertPlace>? {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()
        addLabelToInsertPlaceMap(newP, insertPlaceMap)
        val contentRelation = getContentRelation(newP.getText() ?: "", insertPlaceMap)
        return PsiTemplate(newP, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    override protected fun prepareSubtreeVariants(
              p: ET
            , context: VariantConstructionContext
    ): Map<String, FormatSet> {
        val variants = HashMap<String, FormatSet>()
        prepareLabelVariants(p, variants, context)
        return variants
    }

    protected fun addLabelToInsertPlaceMap(p: ET, insertPlaceMap: MutableMap<String, SmartInsertPlace>): Boolean =
            addLabelToInsertPlaceMap(p, insertPlaceMap, 0)

    protected fun addLabelToInsertPlaceMap(
              p: ET
            , insertPlaceMap: MutableMap<String, SmartInsertPlace>
            , delta: Int
    ): Boolean {
        val identifier = getLabelIdentifier(p)
        if (identifier == null) { return false }
        val sip = identifier.toSmartInsertPlace()
        insertPlaceMap.put(LABEL_IDENTIFIER_TAG, sip)
        return true
    }

    protected fun prepareLabelVariants(
            p: ET, variants: MutableMap<String, FormatSet>, context: VariantConstructionContext
    ) {
        val identifierVariants = getLabelVariants(p, context)
        if (identifierVariants.isEmpty()) { return }
        variants.put(LABEL_IDENTIFIER_TAG, identifierVariants)
    }

    protected fun getLabelVariants(p: ET, context: VariantConstructionContext): FormatSet {
        val identifier = getLabelIdentifier(p)
        if (identifier == null) { return printer.getEmptySet() }
        val variants = printer.getVariants(identifier, context)
        return variants
    }

    override protected fun getTags(p: ET): Set<String> {
        val set = HashSet<String>()
        if (hasElement(getLabelIdentifier(p))) { set.add(LABEL_IDENTIFIER_TAG) }
        return set
    }

    override protected fun isTemplateSuitable(p: ET, tmplt: PsiTemplate<SmartInsertPlace>): Boolean {
        return hasElement(getLabelIdentifier(p)) == tmplt.insertPlaceMap.keySet().contains(LABEL_IDENTIFIER_TAG)
    }

    private fun getLabelIdentifier(p: PsiElement): PsiElement? =
            when(p) {
                is PsiBreakStatement    -> p.getLabelIdentifier()
                is PsiContinueStatement -> p.getLabelIdentifier()
                else -> null
            }
}