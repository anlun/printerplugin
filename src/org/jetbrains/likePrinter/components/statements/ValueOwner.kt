package org.jetbrains.likePrinter.components.statements

import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import com.intellij.psi.PsiElement
import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import com.intellij.psi.PsiExpression
import com.intellij.psi.PsiReturnStatement
import com.intellij.psi.PsiSwitchLabelStatement
import org.jetbrains.likePrinter.util.string.getFillConstant
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext

import org.jetbrains.likePrinter.templateBase.template.PsiTemplateGen
import org.jetbrains.likePrinter.templateBase.template.Template
import org.jetbrains.likePrinter.util.box.Box
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public interface ValueOwner<ET: PsiElement, T: Template<SmartInsertPlace>>
: PsiElementComponent<ET, SmartInsertPlace, T> {
    final val VALUE_TAG: String
        get() = "value"

    protected fun addValueToInsertPlaceMap(p: ET, insertPlaceMap: MutableMap<String, SmartInsertPlace>): Boolean =
            addValueToInsertPlaceMap(p, insertPlaceMap, 0)

    protected fun addValueToInsertPlaceMap(
              p: ET
            , insertPlaceMap: MutableMap<String, SmartInsertPlace>
            , delta: Int
    ): Boolean {
        val value = getValue(p)
        val valueTextRange = value?.getTextRange()
        if (valueTextRange == null) { return false }

        val text = p.getContainingFile()?.getText()
        if (text == null) { return false }
        val fillConstant = text.getFillConstant(valueTextRange)

        insertPlaceMap.put(
                  VALUE_TAG
                , SmartInsertPlace(valueTextRange.shiftRight(delta), fillConstant, Box.getEverywhereSuitable())
        )
        return true
    }

    protected fun prepareValueVariants(
            p: ET, variants: MutableMap<String, FormatSet>, context: VariantConstructionContext
    ) {
        val valueVariants = getValueVariants(p, context)
        if (valueVariants.isEmpty()) { return }
        variants.put(VALUE_TAG, valueVariants)
    }

    protected fun getValueVariants(p: ET, context: VariantConstructionContext): FormatSet {
        val value = getValue(p)
        if (value == null) { return printer.getEmptySet() }
        return printer.getVariants(value, context)
    }

    private fun getValue(p: ET): PsiExpression? =
            when(p) {
                is PsiReturnStatement      -> p.getReturnValue()
                is PsiSwitchLabelStatement -> p.getCaseValue()
                else -> null
            }
}