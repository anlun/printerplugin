package org.jetbrains.likePrinter.components.statements

import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import com.intellij.psi.PsiStatement
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.Template
import com.intellij.psi.PsiElementFactory
import org.jetbrains.likePrinter.printer.Printer

/**
 * User: anlun
 */
abstract public class StatementComponent<ET: PsiStatement, IPT: SmartInsertPlace, T: Template<IPT>>(
        printer: Printer
): PsiElementComponent<ET, IPT, T>(printer) {
    override protected fun getNewElement(text: String, elementFactory: PsiElementFactory): ET? {
        val newP = elementFactory.createStatementFromText(text, null)
        return newP as? ET
    }
}