package org.jetbrains.likePrinter.components.statements

import org.jetbrains.likePrinter.printer.Printer
import com.intellij.psi.PsiTryStatement
import org.jetbrains.likePrinter.templateBase.template.*
import java.util.HashMap
import org.jetbrains.likePrinter.util.psiElement.*
import org.jetbrains.likePrinter.util.string.*
import com.intellij.psi.JavaPsiFacade
import com.intellij.openapi.util.TextRange
import org.jetbrains.format.Format
import org.jetbrains.likePrinter.util.base.InsertPlace
import com.intellij.psi.PsiCatchSection

import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import java.util.HashSet
import org.jetbrains.likePrinter.util.box.Box
import org.jetbrains.format.FormatSet


/**
 * User: anlun
 */

public class TryComponent(
        printer: Printer
): StatementComponent<PsiTryStatement, SmartInsertPlace, Template<SmartInsertPlace>>(printer) {
    final val       RESOURCE_LIST_TAG = "resource list"
    final val           TRY_BLOCK_TAG = "try block"
    final val     CATCH_BLOCK_TRY_TAG = "catch block in try-catch"
    final val       FINALLY_BLOCK_TAG = "finally block"
    final val         FIRST_CATCH_TAG = "first catch"
    final val        SECOND_CATCH_TAG = "second catch"
    final val         CATCH_BLOCK_TAG = "catch code block"
    final val     CATCH_PARAMETER_TAG = "catch parameter"
    final val CATCH_BLOCK_FINALLY_TAG = "catch block in catch-finally"

    private inner class TryWithCatchSectionsTemplate(
                                psi: PsiTryStatement
            , val     tryCatchTmplt: Template<SmartInsertPlace>
            , val        catchTmplt: Template<SmartInsertPlace>
            , val   catchCatchTmplt: Template<SmartInsertPlace>?
            , val catchFinallyTmplt: Template<SmartInsertPlace>?
    ): Template<SmartInsertPlace>(
              JUNK_TEXT
            , getFullConstructionInsertPlaceMap         ()
            , getFullConstructionTagPlaceToLineNumberMap()
            , getFullConstructionLineEquationMap        ()
    ) {
        val t1: String? = psi.getText()

        override public fun toString(): String {
            val builder = StringBuilder()
            builder.append(tryCatchTmplt.toString())
            builder.append(   catchTmplt.toString())
            if (  catchCatchTmplt != null) { builder.append(  catchCatchTmplt.toString()) }
            if (catchFinallyTmplt != null) { builder.append(catchFinallyTmplt.toString()) }
            return builder.toString()
        }
    }

    override public fun getTemplateFromElement(newP: PsiTryStatement): Template<SmartInsertPlace>? {
        val catchSections = newP.getCatchSections()
        if (catchSections.isNotEmpty()) {
            return getTryWithCatchTemplate(newP)
        }

        val insertPlaceMap = HashMap<String, SmartInsertPlace>()

        val resourceList = newP.getResourceList()
        if (resourceList != null) {
            insertPlaceMap.put(RESOURCE_LIST_TAG, resourceList.toSmartInsertPlace())
        }

        val tryBlockSIP = getCodeBlockInsertPlace(newP.getTryBlock())
        if (tryBlockSIP == null) { return null }
        insertPlaceMap.put(TRY_BLOCK_TAG, tryBlockSIP)

        val finallyBlockSIP = getCodeBlockInsertPlace(newP.getFinallyBlock())
        if (finallyBlockSIP != null) {
            insertPlaceMap.put(FINALLY_BLOCK_TAG, finallyBlockSIP)
        }

        val contentRelation = getContentRelation(newP.getText() ?: "", insertPlaceMap)
        return PsiTemplate(newP, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    private fun getTryWithCatchTemplate(newP: PsiTryStatement): TryWithCatchSectionsTemplate? {
        val tryCatchTemplate = getTryCatchTemplate(newP)
        if (tryCatchTemplate == null) { return null }

        val catchTemplate = getCatchTemplate(newP)
        if (catchTemplate == null) { return null }

        val catchCatchTemplate   = getCatchCatchTemplate(newP)
        val catchFinallyTemplate = getCatchFinallyTemplate(newP)

        return TryWithCatchSectionsTemplate(
                  newP
                , tryCatchTemplate
                , catchTemplate
                , catchCatchTemplate
                , catchFinallyTemplate
        )
    }

    private fun getTryCatchTemplate(newP: PsiTryStatement): Template<SmartInsertPlace>? {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()

        val resourceList = newP.getResourceList()
        if (resourceList != null) {
            insertPlaceMap.put(RESOURCE_LIST_TAG, resourceList.toSmartInsertPlace())
        }

        val tryBlockSIP = getCodeBlockInsertPlace(newP.getTryBlock())
        if (tryBlockSIP == null) { return null }
        insertPlaceMap.put(TRY_BLOCK_TAG, tryBlockSIP)

        val catchSections = newP.getCatchSections()
        if (catchSections.isEmpty()) { return null }
        val firstCatch = catchSections[0]
        val catchRange = firstCatch.getNotNullTextRange()
        insertPlaceMap.put(CATCH_BLOCK_TRY_TAG, firstCatch.toSmartInsertPlace())

        val text = newP.getText()?.substring(0, catchRange.getEndOffset()) ?: ""
        val contentRelation = getContentRelation(text, insertPlaceMap)

        return Template(text, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    private fun getCatchTemplate(p: PsiTryStatement): Template<SmartInsertPlace>? {
        val catchSections = p.getCatchSections()
        if (catchSections.isEmpty()) { return null }
        val firstCatch = catchSections[0]

        val prefixToStmt = "try{}\n"
        val catchText = firstCatch.deleteSpaces()
        val text = prefixToStmt + catchText
        val elementFactory = JavaPsiFacade.getElementFactory(printer.getProject())
        val newTryBlock = elementFactory?.createStatementFromText(text, null)
        if (newTryBlock !is PsiTryStatement) { return null }
        val newP = newTryBlock.getCatchSections()[0]

        val insertPlaceMap = HashMap<String, SmartInsertPlace>()
        val negShift = -prefixToStmt.length()

        val parameter = newP.getParameter()
        if (parameter == null) { return null }
        insertPlaceMap.put(CATCH_PARAMETER_TAG
                    , SmartInsertPlace(
                          parameter.getTextRange()?.shiftRight(negShift) ?: TextRange(0, 0)
                        , parameter.getFillConstant()
                        , Box.getEverywhereSuitable()
                    )
        )

        val catchBlockSIP_withoutShift = getCodeBlockInsertPlace(newP.getCatchBlock())
        val catchBlockSIP = catchBlockSIP_withoutShift?.shiftRight(negShift)
        if (catchBlockSIP == null) { return null }
        insertPlaceMap.put(CATCH_BLOCK_TAG, catchBlockSIP)

        val contentRelation = getContentRelation(catchText, insertPlaceMap)
        return Template(catchText, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    private fun getCatchCatchTemplate(newP: PsiTryStatement): Template<SmartInsertPlace>? {
        val catchSections = newP.getCatchSections()
        if (catchSections.size() < 2) { return null }
        val  firstCatch = catchSections[0]
        val secondCatch = catchSections[1]

        val firstCatchRange  = firstCatch .getNotNullTextRange()
        val secondCatchRange = secondCatch.getNotNullTextRange()

        val textRange = firstCatchRange.union(secondCatchRange)
        val text = newP.getText()?.substring(textRange) ?: ""

        val insertPlaceMap = HashMap<String, SmartInsertPlace>()
        val negShift = -firstCatchRange.getStartOffset()

        val firstCatchSIP = SmartInsertPlace(
                  firstCatchRange.shiftRight(negShift)
                , firstCatch.getFillConstant(), Box.getEverywhereSuitable()
        )
        insertPlaceMap.put(FIRST_CATCH_TAG, firstCatchSIP)

        val secondCatchSIP = SmartInsertPlace(
                  secondCatchRange.shiftRight(negShift)
                , secondCatch.getFillConstant(), Box.getEverywhereSuitable())
        insertPlaceMap.put(SECOND_CATCH_TAG, secondCatchSIP)

        val contentRelation = getContentRelation(text, insertPlaceMap)
        return Template(text, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    private fun getCatchFinallyTemplate(newP: PsiTryStatement): Template<SmartInsertPlace>? {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()

        val catchSections = newP.getCatchSections()
        if (catchSections.isEmpty()) { return null }
        val lastCatch = catchSections[catchSections.lastIndex]
        val catchRange = lastCatch.getNotNullTextRange()
        val negShift = -catchRange.getStartOffset()
        insertPlaceMap.put(CATCH_BLOCK_FINALLY_TAG, lastCatch.toSmartInsertPlace().shiftRight(negShift))

        val finallyBlockSIP = getCodeBlockInsertPlace(newP.getFinallyBlock())
        if (finallyBlockSIP == null) { return null }
        insertPlaceMap.put(FINALLY_BLOCK_TAG, finallyBlockSIP.shiftRight(negShift))

//        val text = newP.getText()?.substring(0, catchRange.getEndOffset()) ?: ""
        val text = newP.getText()?.substring(catchRange.union(finallyBlockSIP.range)) ?: ""

        val contentRelation = getContentRelation(text, insertPlaceMap)
        return Template(text, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    override protected fun isTemplateSuitable(p: PsiTryStatement, tmplt: Template<SmartInsertPlace>): Boolean {
        val catchSections = p.getCatchSections()
        if (catchSections.isNotEmpty() != tmplt is TryWithCatchSectionsTemplate) { return false }
//        if (catchSections.isNotEmpty()) {
//            if (tmplt is TryWithCatchSectionsTemplate) {
//                val hasFinallyBlock = p.getFinallyBlock() == null
//                val tmpltHasFinallyBlock = tmplt.catchFinallyTmplt == null
//                if (hasFinallyBlock != tmpltHasFinallyBlock) { return false }
//
//                if (catchSections.size > 1 && tmplt.catchCatchTmplt == null) { return false }
//
//            }
//            return false
//        }
        return true
    }

    override protected fun prepareSubtreeVariants(
              p: PsiTryStatement
            , context: VariantConstructionContext
    ): Map<String, FormatSet> {
        val variants = HashMap<String, FormatSet>()
        val catchSections = p.getCatchSections()
        if (catchSections.isNotEmpty()) { return variants }

        val resourceListVariants = getResourceListVariants(p, context)
        if (resourceListVariants.isEmpty()) { return variants }
        variants.put(RESOURCE_LIST_TAG, resourceListVariants)

        return variants
    }

    protected fun getResourceListVariants(p: PsiTryStatement, context: VariantConstructionContext): FormatSet {
        val resourceList = p.getResourceList()
        if (resourceList == null) { return printer.getEmptySet() }
        return printer.getVariants(resourceList, context)
    }

    protected fun getTryBlockVariants(
                    p: PsiTryStatement
            ,   tmplt: Template<SmartInsertPlace>
            , context: VariantConstructionContext
    ): FormatSet =
        getCodeBlockPartVariants(
                  p.getTryBlock()
                , TRY_BLOCK_TAG
                , tmplt
                , context
        )

    protected fun getFinallyBlockVariants(
                    p: PsiTryStatement
            ,   tmplt: Template<SmartInsertPlace>
            , context: VariantConstructionContext
    ): FormatSet =
        getCodeBlockPartVariants(
                  p.getFinallyBlock()
                , FINALLY_BLOCK_TAG
                , tmplt
                , context
        )

    protected fun getFullConstructionVariants(
                    p: PsiTryStatement
            ,   tmplt: Template<SmartInsertPlace>
            , context: VariantConstructionContext
    ): FormatSet {
        //TODO: use context widthToSuit info!!!

        val catchSections = p.getCatchSections()
        if (tmplt !is TryWithCatchSectionsTemplate) { return printer.getEmptySet() }

        val catchSectionVariants = catchSections.map { cs ->
            getCatchSectionVariantsByTemplate(cs, tmplt.catchTmplt, context)
        }
        val lastCatchVariants = catchSectionVariants.lastOrNull() ?: return printer.getEmptySet()
        var resultVariants: FormatSet = lastCatchVariants

        val catchFinallyTmplt = tmplt.catchFinallyTmplt
        if (catchFinallyTmplt != null) {
            val catchFinallyVariants = getCatchFinallyVariantsByTemplate(p, lastCatchVariants, catchFinallyTmplt, context)
            if (catchFinallyVariants.isEmpty()) { return printer.getEmptySet() }
            resultVariants = catchFinallyVariants
        } else {
            if (p.getFinallyBlock() != null) {
                return printer.getEmptySet()
            }
        }

        if (catchSections.size() > 1) {
            val ccTmplt = tmplt.catchCatchTmplt
            if (ccTmplt == null) { return printer.getEmptySet() }

            for (i in (catchSectionVariants.lastIndex - 1) downTo 0) {
                resultVariants = getCatchCatchVariantsByTemplate(catchSectionVariants[i], resultVariants, ccTmplt)
            }
        }

        resultVariants = getTryCatchVariantsByTemplate(
                  p
                , getResourceListVariants(p, context)
                , resultVariants
                , tmplt.tryCatchTmplt
                , context
        )

        return resultVariants
    }

    override protected fun updateSubtreeVariants(
                     p: PsiTryStatement
            ,    tmplt: Template<SmartInsertPlace>
            , variants: Map<String, FormatSet>
            ,  context: VariantConstructionContext
    ): Map<String, FormatSet> {
        val catchSections = p.getCatchSections()
        if (catchSections.isEmpty()) {
            //variants.toMap(newVariants)
            val newVariants = HashMap(variants)
            updateCodeBlockPart(p.getTryBlock    (),     TRY_BLOCK_TAG, tmplt, newVariants, context)
            updateCodeBlockPart(p.getFinallyBlock(), FINALLY_BLOCK_TAG, tmplt, newVariants, context)

            return newVariants
        }

//        val newVariants = HashMap<String, FormatSet>()
        val resultVariants = getFullConstructionVariants(p, tmplt, context)
//        newVariants.put(FULL_CONSTRUCTION_TAG, resultVariants)

        return mapOf(FULL_CONSTRUCTION_TAG to resultVariants) // newVariants
    }

    override protected fun getTags(p: PsiTryStatement): Set<String> {
        val catchSections = p.getCatchSections()
        if (catchSections.isNotEmpty()) { return setOf(FULL_CONSTRUCTION_TAG) }

        val set = HashSet<String>()
        if (hasElement(p.getResourceList())) { set.add(RESOURCE_LIST_TAG) }
        if (hasElement(p.getFinallyBlock())) { set.add(FINALLY_BLOCK_TAG) }
        set.add(TRY_BLOCK_TAG)

        return set
    }

    private fun getTryCatchVariantsByTemplate(
                             p: PsiTryStatement
            , resourceVariants: FormatSet
            ,    catchVariants: FormatSet
            ,            tmplt: Template<SmartInsertPlace>
            ,          context: VariantConstructionContext
    ): FormatSet {
        val variants = HashMap<String, FormatSet>()

        if (resourceVariants.isNotEmpty()) { variants.put(RESOURCE_LIST_TAG, resourceVariants) }

        variants.put(CATCH_BLOCK_TRY_TAG, catchVariants)
        updateCodeBlockPart(p.getTryBlock(), TRY_BLOCK_TAG, tmplt, variants, context)

        return getVariants(tmplt.text, tmplt.insertPlaceMap, variants) ?: printer.getEmptySet()
    }


    private fun getCatchSectionVariantsByTemplate(
            p: PsiCatchSection, tmplt: Template<SmartInsertPlace>, context: VariantConstructionContext
    ): FormatSet {
        val variants = HashMap<String, FormatSet>()

        val parameter = p.getParameter()
        if (parameter == null) { return printer.getEmptySet() }
        val parameterVariants = printer.getVariants(parameter, context)
        variants.put(CATCH_PARAMETER_TAG, parameterVariants)

        updateCodeBlockPart(p.getCatchBlock(), CATCH_BLOCK_TAG, tmplt, variants, context)

        return getVariants(tmplt.text, tmplt.insertPlaceMap, variants) ?: printer.getEmptySet()
    }

    private fun getCatchCatchVariantsByTemplate(
               firstCatchVariants: FormatSet
            , secondCatchVariants: FormatSet
            , tmplt: Template<SmartInsertPlace>
    ): FormatSet {
        val variants = HashMap<String, FormatSet>()
        variants.put( FIRST_CATCH_TAG,  firstCatchVariants)
        variants.put(SECOND_CATCH_TAG, secondCatchVariants)

        return getVariants(tmplt.text, tmplt.insertPlaceMap, variants) ?: printer.getEmptySet()
    }

    private fun getCatchFinallyVariantsByTemplate(
                              p: PsiTryStatement
            , lastCatchVariants: FormatSet
            ,             tmplt: Template<SmartInsertPlace>
            ,           context: VariantConstructionContext
    ): FormatSet {
        val variants = HashMap<String, FormatSet>()
        variants.put(CATCH_BLOCK_FINALLY_TAG, lastCatchVariants)
        updateCodeBlockPart(p.getFinallyBlock(), FINALLY_BLOCK_TAG, tmplt, variants, context)

        return getVariants(tmplt.text, tmplt.insertPlaceMap, variants) ?: printer.getEmptySet()
    }
}