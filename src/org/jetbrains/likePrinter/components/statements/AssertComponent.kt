package org.jetbrains.likePrinter.components.statements

import org.jetbrains.likePrinter.printer.Printer
import com.intellij.psi.PsiAssertStatement
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import org.jetbrains.likePrinter.components.EmptyUpdateComponent
import org.jetbrains.likePrinter.components.ConditionOwner
import java.util.HashMap
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext

import org.jetbrains.likePrinter.util.psiElement.toSmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiTemplateGen
import org.jetbrains.format.emptyFormatList
import java.util.HashSet
import org.jetbrains.format.FormatSet


/**
 * User: anlun
 */
public class AssertComponent(
        printer: Printer
): StatementComponent
    <PsiAssertStatement, SmartInsertPlace, PsiTemplateGen<PsiAssertStatement, SmartInsertPlace>>(printer)
 , ConditionOwner
    <PsiAssertStatement, SmartInsertPlace, PsiTemplateGen<PsiAssertStatement, SmartInsertPlace>>
 , EmptyUpdateComponent
    <PsiAssertStatement, SmartInsertPlace, PsiTemplateGen<PsiAssertStatement, SmartInsertPlace>>
{
    final val DESCRIPTION_TAG: String
        get() = "description"

    override public fun getTemplateFromElement(
            newP: PsiAssertStatement
    ): PsiTemplateGen<PsiAssertStatement, SmartInsertPlace>? {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()

        addConditionToInsertPlaceMap(newP, insertPlaceMap)

        val description = newP.getAssertDescription()
        if (description == null) { return null }
        val sip = description.toSmartInsertPlace()
        insertPlaceMap.put(DESCRIPTION_TAG, sip)

        val contentRelation = getContentRelation(newP.getText() ?: "", insertPlaceMap)
        return PsiTemplateGen(newP, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    override protected fun prepareSubtreeVariants(
              p: PsiAssertStatement
            , context: VariantConstructionContext
    ): Map<String, FormatSet> {
        val variants = HashMap<String, FormatSet>()

        prepareConditionPart(p, variants, context)

        val descriptionVariants = getDescriptionVariants(p, context)
        if (descriptionVariants.isEmpty()) { return variants }
        variants.put(DESCRIPTION_TAG, descriptionVariants)

        return variants
    }

    protected fun getDescriptionVariants(p: PsiAssertStatement, context: VariantConstructionContext): FormatSet {
        val description = p.getAssertDescription()
        if (description == null) { return printer.getEmptySet() }
        val variants = printer.getVariants(description, context)
        return variants
    }

    override protected fun getTags(p: PsiAssertStatement): Set<String> {
        val set = HashSet<String>()
        set.add(CONDITION_TAG)
        if (p.getAssertDescription() != null) { set.add(DESCRIPTION_TAG) }
        return set
    }

    override protected fun isTemplateSuitable(
            p: PsiAssertStatement, tmplt: PsiTemplateGen<PsiAssertStatement, SmartInsertPlace>
    ) = (p.getAssertDescription() != null) == (tmplt.psi.getAssertDescription() != null)
}