package org.jetbrains.likePrinter.components.statements

import com.intellij.psi.PsiSwitchStatement
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import org.jetbrains.likePrinter.printer.Printer
import com.intellij.openapi.project.Project
import java.util.HashMap
import org.jetbrains.likePrinter.util.psiElement.toSmartInsertPlace
import org.jetbrains.likePrinter.util.base.IncorrectPsiElementException
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext

import java.util.HashSet
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent.CodeBlockSpecialTemplateInsertPlace
import com.intellij.psi.PsiCodeBlock
import com.intellij.psi.PsiBlockStatement
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */

public class SwitchComponent(
        printer: Printer
): StatementComponent<PsiSwitchStatement, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>(printer) {
    final val EXPRESSION_TAG = "expression"

    override public fun getTemplateFromElement(newP: PsiSwitchStatement): PsiTemplate<SmartInsertPlace>? {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()

        val expression = newP.getExpression()
        if (expression == null) { return null }
        val expressionSIP = expression.toSmartInsertPlace()
        insertPlaceMap.put(EXPRESSION_TAG, expressionSIP)

        if (!addBodyToInsertPlaceMap(newP, insertPlaceMap)) { return null }

        val contentRelation = getContentRelation(newP.getText() ?: "", insertPlaceMap)
        return PsiTemplate(newP, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    protected fun addBodyToInsertPlaceMap(p: PsiSwitchStatement, insertPlaceMap: MutableMap<String, SmartInsertPlace>): Boolean =
            addBodyToInsertPlaceMap(p, insertPlaceMap, 0)

    protected fun addBodyToInsertPlaceMap(
              p: PsiSwitchStatement
            , insertPlaceMap: MutableMap<String, SmartInsertPlace>
            , delta: Int
    ): Boolean {
        val bodySIP = getCodeBlockInsertPlace(p.getBody())
        if (bodySIP == null) { return false }
        insertPlaceMap.put(BODY_TAG, bodySIP.shiftRight(delta))
        return true
    }

    override protected fun prepareSubtreeVariants(
            p: PsiSwitchStatement, context: VariantConstructionContext
    ): Map<String, FormatSet> {
        val variants = HashMap<String, FormatSet>()

        val expressionVariants = getExpressionVariants(p, context)
        if (expressionVariants.isEmpty()) { return variants }
        variants.put(EXPRESSION_TAG, expressionVariants)

        return variants
    }

    protected fun getExpressionVariants(p: PsiSwitchStatement, context: VariantConstructionContext): FormatSet {
        val expression = p.getExpression()
        if (expression == null) { return printer.getEmptySet() }
        return printer.getVariants(expression, context)
    }

    protected fun getBodyVariants(p: PsiSwitchStatement, tmplt: PsiTemplate<SmartInsertPlace>, context: VariantConstructionContext): FormatSet =
            getCodeBlockPartVariants(p.getBody(), BODY_TAG, tmplt, context)

    override protected fun updateSubtreeVariants(
              p       : PsiSwitchStatement
            , tmplt   : PsiTemplate<SmartInsertPlace>
            , variants: Map<String, FormatSet>
            , context : VariantConstructionContext
    ): Map<String, FormatSet> {
        val newVariants = HashMap<String, FormatSet>(variants)
        //variants.toMap(newVariants)
        updateCodeBlockPart(p.getBody(), BODY_TAG, tmplt, newVariants, context)
        return newVariants
    }

    override protected fun getTags(p: PsiSwitchStatement) = setOf(EXPRESSION_TAG, BODY_TAG)

    override protected fun isTemplateSuitable(p: PsiSwitchStatement, tmplt: PsiTemplate<SmartInsertPlace>): Boolean {
        val body = p.getBody()
        val bodyTmplt = tmplt.insertPlaceMap.get(BODY_TAG)

        return (bodyTmplt is CodeBlockSpecialTemplateInsertPlace) == (body is PsiBlockStatement || body is PsiCodeBlock)
    }
}