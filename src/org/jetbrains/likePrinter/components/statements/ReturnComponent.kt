package org.jetbrains.likePrinter.components.statements

import org.jetbrains.likePrinter.printer.Printer
import com.intellij.psi.PsiReturnStatement
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import org.jetbrains.likePrinter.components.EmptyUpdateComponent
import java.util.HashMap
import org.jetbrains.likePrinter.util.psiElement.toSmartInsertPlace
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext

import org.jetbrains.likePrinter.util.psiElement.getCorrectTextOffset
import java.util.HashSet
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public class ReturnComponent(
        printer: Printer
): StatementComponent  <PsiReturnStatement, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>(printer)
 , ValueOwner          <PsiReturnStatement, PsiTemplate<SmartInsertPlace>>
 , EmptyUpdateComponent<PsiReturnStatement, SmartInsertPlace, PsiTemplate<SmartInsertPlace>> {

    override public fun getTemplateFromElement(newP: PsiReturnStatement): PsiTemplate<SmartInsertPlace>? {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()

        val negShift = -newP.getCorrectTextOffset()
        addValueToInsertPlaceMap(newP, insertPlaceMap, negShift)

        val contentRelation = getContentRelation(newP.getText() ?: "", insertPlaceMap)
        return PsiTemplate(newP, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    override protected fun prepareSubtreeVariants(
                    p: PsiReturnStatement
            , context: VariantConstructionContext
    ): Map<String, FormatSet> {
        val variants = HashMap<String, FormatSet>()
        prepareValueVariants(p, variants, context)
        return variants
    }

    override protected fun getTags(p: PsiReturnStatement): Set<String> {
        if (p.getReturnValue() == null) { return HashSet() }
        return setOf(VALUE_TAG)
    }

    override protected fun isTemplateSuitable(p: PsiReturnStatement, tmplt: PsiTemplate<SmartInsertPlace>): Boolean {
        val text = p.getText() ?: ""
        return text.endsWith(';') == tmplt.text.endsWith(';')
    }
}