package org.jetbrains.likePrinter.components.statements

import com.intellij.psi.PsiForStatement
import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.util.psiElement.*

import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import java.util.HashSet
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */

public class ForComponent(
        printer: Printer
): ConditionLoopComponent<PsiForStatement>(printer) {
    final val   INIT_TAG = "initialization"
    final val UPDATE_TAG = "update"

    override protected fun addToInsertPlaceMapSpecificElements(
              p: PsiForStatement
            , insertPlaceMap: MutableMap<String, SmartInsertPlace>
    ): Boolean {
        val initialization = p.getInitialization()
        if (initialization != null) {
            insertPlaceMap.put(INIT_TAG, initialization.toSmartInsertPlace())
        }

        val update = p.getUpdate()
        if (update != null) {
            insertPlaceMap.put(UPDATE_TAG, update.toSmartInsertPlace())
        }

        return true
    }

    override protected fun prepareSpecificParts(
              p: PsiForStatement
            , variants: MutableMap<String, FormatSet>
            , context: VariantConstructionContext
    ) {
        val initVariants = getInitVariants(p, context)
        if (!initVariants.isEmpty()) { variants.put(INIT_TAG, initVariants) }

        val updateVariants = getUpdateVariants(p, context)
        if (!updateVariants.isEmpty()) { variants.put(UPDATE_TAG, updateVariants) }
    }

    private fun getInitVariants(p: PsiForStatement, context: VariantConstructionContext): FormatSet {
        val initialization = p.getInitialization()
        if (initialization == null) { return printer.getEmptySet() }
        return printer.getVariants(initialization, context)
    }

    private fun getUpdateVariants(p: PsiForStatement, context: VariantConstructionContext): FormatSet {
        val update = p.getUpdate()
        if (update == null) { return printer.getEmptySet() }
        return printer.getVariants(update, context)
    }

    override protected fun getTags(p: PsiForStatement): Set<String> {
        val set = HashSet<String>()
        if (hasElement(p.getInitialization())) { set.add(     INIT_TAG) }
        if (hasElement(p.getUpdate        ())) { set.add(   UPDATE_TAG) }
        if (hasElement(p.getCondition     ())) { set.add(CONDITION_TAG) }
        addPossibleCodeBlockTag(set, p.getBody(), BODY_TAG)
        return set
    }
}