package org.jetbrains.likePrinter.components

import com.intellij.psi.PsiElement
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.Template
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import com.intellij.psi.PsiNewExpression
import com.intellij.openapi.util.TextRange
import com.intellij.psi.impl.PsiImplUtil
import com.intellij.psi.JavaTokenType
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import org.jetbrains.likePrinter.util.string.getFillConstant
import com.intellij.lang.ASTNode
import com.intellij.psi.PsiWhiteSpace
import com.intellij.psi.PsiComment
import java.util.ArrayList
import org.jetbrains.likePrinter.util.box.Box
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public interface ArrayDimensionOwner<ET: PsiElement, IPT: SmartInsertPlace, T: Template<IPT>>: PsiElementComponent<ET, IPT, T> {

    final val ARRAY_DIMENSION_TAG: String
        get() = "array dimensions"

    private fun PsiElement.hasArrayDimensions(): Boolean = getArrayDimensionList()?.isNotEmpty() ?: false

    private fun PsiElement.getArrayDimensionRange(): TextRange? {
        val result = getArrayDimensionList()?.fold(null: TextRange?) { result, pair ->
            val firstRange  = pair.first .getTextRange()
            val secondRange = pair.second.getTextRange()
            val unionRange  = if (secondRange != null) { firstRange?.union(secondRange) } else { firstRange }
            if (result != null) {
                if (unionRange != null) {
                    result.union(unionRange)
                } else {
                    result
                }
            } else {
                unionRange
            }
        }

        return result
    }

    protected fun PsiElement.getArrayDimensionList(): List<Pair<ASTNode, ASTNode>>? {
        val children = getChildren()
        val filteredChildren = children filterNot { ch -> ch is PsiWhiteSpace || ch is PsiComment }
        val childNodes  = filteredChildren map { ch -> ch.getNode() }
        if (childNodes.isEmpty()) { return null }

        var currentRbracket: ASTNode? = null
        val result = ArrayList<Pair<ASTNode, ASTNode>>()
        for (currentChild in (0..childNodes.size()-1).reversed()) {
            val currentNode = childNodes.get(currentChild)
            if (currentNode == null) { return result }

            if (currentNode.getElementType() == JavaTokenType.RBRACKET) {
                if (currentRbracket != null) { return null }
                currentRbracket = currentNode
                continue
            }
            if (currentNode.getElementType() == JavaTokenType.LBRACKET) {
                val leftBracket = currentRbracket
                if (leftBracket == null) { return null }
                result.add(Pair<ASTNode, ASTNode>(currentNode, leftBracket))
                currentRbracket = null
                continue
            }

//            (currentNode.getElementType() != JavaTokenType.LBRACKET)
//                &&
//            (currentNode.getElementType() != JavaTokenType.RBRACKET)
            if (currentRbracket == null) {
                break
            }
        }

        return result
    }

    protected fun addArrayDimensionToInsertPlaceMap(p: ET, insertPlaceMap: MutableMap<String, SmartInsertPlace>): Boolean =
            addArrayDimensionToInsertPlaceMap(p, insertPlaceMap, 0)

    protected fun addArrayDimensionToInsertPlaceMap(
              p: ET
            , insertPlaceMap: MutableMap<String, SmartInsertPlace>
            , delta: Int
    ): Boolean {
        if (!p.hasArrayDimensions()) { return false }
        val range = p.getArrayDimensionRange()
        if (range == null) { return false }
        val fillConstant = p.getContainingFile()?.getText()?.getFillConstant(range) ?: 0
        insertPlaceMap.put(
                  ARRAY_DIMENSION_TAG
                , SmartInsertPlace(range, fillConstant, Box.getEverywhereSuitable()).shiftRight(delta)
        )
        return true
    }

    protected fun prepareArrayDimensionVariants(
            p: ET, variants: MutableMap<String, FormatSet>, context: VariantConstructionContext
    ) {
        if (!p.hasArrayDimensions()) { return }
        variants.put(ARRAY_DIMENSION_TAG, getArrayDimensionVariants(p, context))
    }


    protected fun getArrayDimensionVariants(p: ET, context: VariantConstructionContext): FormatSet
}