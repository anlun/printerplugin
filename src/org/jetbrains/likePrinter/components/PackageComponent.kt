package org.jetbrains.likePrinter.components

import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import com.intellij.psi.PsiPackageStatement
import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import com.intellij.psi.PsiElementFactory
import org.jetbrains.likePrinter.templateBase.template.Template
import java.util.HashMap
import org.jetbrains.likePrinter.util.psiElement.toSmartInsertPlace
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext

import java.util.HashSet
import org.jetbrains.likePrinter.util.psiElement.hasElement
import org.jetbrains.likePrinter.util.psiElement.getCorrectTextOffset
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public class PackageComponent(
        printer: Printer
): PsiElementComponent     <PsiPackageStatement, SmartInsertPlace, Template<SmartInsertPlace>>(printer)
 , EmptyNewElementComponent<PsiPackageStatement, SmartInsertPlace, Template<SmartInsertPlace>>
 , ReferenceOwner          <PsiPackageStatement, SmartInsertPlace, Template<SmartInsertPlace>>
 , EmptyUpdateComponent    <PsiPackageStatement, SmartInsertPlace, Template<SmartInsertPlace>> {

    override public fun getTemplateFromElement(newP: PsiPackageStatement): Template<SmartInsertPlace>? {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()
        val negShift = -newP.getCorrectTextOffset()
        addReferenceToInsertPlaceMap(newP, insertPlaceMap, negShift)
        val contentRelation = getContentRelation(newP.getText() ?: "", insertPlaceMap)
        return Template(newP.getText() ?: "", insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    override protected fun prepareSubtreeVariants(
                    p: PsiPackageStatement
            , context: VariantConstructionContext
    ): Map<String, FormatSet> {
        val variants = HashMap<String, FormatSet>()
        prepareReferenceVariants(p, variants, context)
        return variants
    }

    override protected fun getTags(p: PsiPackageStatement): Set<String> {
        val set = HashSet<String>()

        val packageReference = p.getPackageReference()
        if (hasElement(packageReference)) { set.add(REFERENCE_TAG) }

        return set
    }

    override protected fun isTemplateSuitable(
            p: PsiPackageStatement, tmplt: Template<SmartInsertPlace>
    ): Boolean = true
}