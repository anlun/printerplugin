package org.jetbrains.likePrinter

import com.intellij.openapi.actionSystem.AnAction
import org.jetbrains.likePrinter.printer.Printer
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys
import com.intellij.psi.PsiFileFactory
import org.jetbrains.likePrinter.ui.PrinterInitDialog
import java.lang.management.ManagementFactory
import java.util.ArrayList
import org.jetbrains.likePrinter.ui.SubtreeShower
import com.intellij.lang.java.JavaLanguage

/**
 * User: anlun
 */
public class CodeFormatAction : AnAction(), PrinterOwner {
    private var myPrinter: Printer? = null
    companion object {
        private val ourNeedsToShowPerformanceDialog = false
    }

    override public fun getPrinter() = myPrinter
    override public fun setPrinter(printer: Printer) { myPrinter = printer }
    override public fun actionPerformed(e: AnActionEvent) {
        val project = e.getData(CommonDataKeys.PROJECT)
        val psiJavaFile = e.getPsiJavaFileFromContext() ?: return //TODO: show message for user!!!!!
        val factory = PsiFileFactory.getInstance(project)

        val initDialog = PrinterInitDialog(this, project, factory)
        initDialog.pack()
        initDialog.setVisible(true)

        val printer = getPrinter()
        if (printer == null || !printer.areTemplatesFilled()) { return }
        printer.clearLog()

        val threadMXBean = ManagementFactory.getThreadMXBean()
        val startTime = threadMXBean.getCurrentThreadCpuTime()
        printer.reprint(psiJavaFile)
        val endTime = threadMXBean.getCurrentThreadCpuTime()

        if (ourNeedsToShowPerformanceDialog) { showPerformanceDialog(endTime - startTime, printer) }
    }

    private fun showPerformanceDialog(duration: Long, printer: Printer) {
        val log = ArrayList<String>() //printer.getLog()
        log.add("Duration: " + duration / Math.pow(10.0, 9.0))
        log.add("Miss: " + printer.cacheMissCount)
        log.add("Used: " + printer.memorizedVariantUseCount)
        log.add("ReplaceTime: " + printer.replaceTime / Math.pow(10.0, 9.0))
        val shower = SubtreeShower(log)
        shower.pack()
        shower.setVisible(true)
    }

    override public fun update(e: AnActionEvent) {
        val project = e.getData(CommonDataKeys.PROJECT)
        val editor = e.getData(CommonDataKeys.EDITOR)
        val psiFile = e.getData(CommonDataKeys.PSI_FILE)
        val presentation = e.getPresentation()

        if (psiFile == null || project == null || editor == null) {
            presentation.setEnabled(false)
            return
        }

        val isLangJava = psiFile.getLanguage().equals(JavaLanguage.INSTANCE)
        presentation.setEnabled(isLangJava)
    }
}