package org.jetbrains.likePrinter

import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.PlatformDataKeys
import com.intellij.psi.PsiDocumentManager
import com.intellij.openapi.actionSystem.LangDataKeys
import com.intellij.lang.java.JavaLanguage
import com.intellij.openapi.actionSystem.CommonDataKeys

/**
 * User: anlun
 */
public class FlatFormatAction: AnAction() {
  override fun actionPerformed(e: AnActionEvent) {
    val project = e.getData(CommonDataKeys.PROJECT) ?: return
    val psiJavaFile = e.getPsiJavaFileFromContext() ?: return
    val document = PsiDocumentManager.getInstance(project).getDocument(psiJavaFile) ?: return
    //TODO: show message for user in case of document, project or psiJavaFile are equal to null

    val text = document.getText()
    val newText = text.replace('\n', ' ')
    project.performUndoWrite { document.replaceString(0, text.length(), newText) }
  }

  override fun update(e: AnActionEvent) {
    val project = e.getData(CommonDataKeys.PROJECT)
    val  editor = e.getData(CommonDataKeys.EDITOR)
    val psiFile = e.getData(CommonDataKeys.PSI_FILE)
    val presentation = e.getPresentation()

    if (psiFile == null || project == null || editor == null) {
      presentation.setEnabled(false)
      return
    }

    val isLangJava = psiFile.getLanguage().equals(JavaLanguage.INSTANCE)
    presentation.setEnabled(isLangJava)
  }
}